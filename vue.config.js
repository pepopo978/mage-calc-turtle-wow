// vue.config.js file to be place in the root of your repository
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const os = require('os')

module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',
  chainWebpack: config => {
    config.plugin('fork-ts-checker').tap(args => {
      const totalmem = Math.floor(os.totalmem() / 1024 / 1024) //get OS mem size
      const allowUseMem = totalmem > 5000 ? 4096 : 2048
      // in vue-cli shuld args[0]['typescript'].memoryLimit
      if (args[0].hasOwnProperty('typescript')) {
        args[0].typescript.memoryLimit = allowUseMem
      } else {
        args[0].memoryLimit = allowUseMem
      }

      return args
    })
  }
}
process.env.VUE_APP_VERSION = require('./package.json').version
