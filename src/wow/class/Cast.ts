import Character from './Character'
import OptionsInterface from '../interface/Options'
import Spell from './Spell'
import Target from './Target'
import Equipment from './Equipment'
import MagicSchool from '../enum/MagicSchool'
import { BASE_SPELL_CRIT_MULTIPLIER, GLOBAL_COOLDOWN, SPELL_CRIT_CAP } from '../module/Constants'
import ManaData from '../interface/ManaData'

/**
 * A Spell cast by Character at Target.
 */
export default class Cast {
  spell: Spell
  target: Target
  character: Character
  options: OptionsInterface

  constructor(options: OptionsInterface, equipment?: Equipment) {
    /* By default, gear is determined by Equipment(). We can override it by passing our own in.
     * If we don't pass our own equipment in, we can also override the stat weights used
     * by Equipment() to select the gear */

    this.options = options
    this.character = new Character(options.character, equipment ? equipment : new Equipment(options))
    this.spell = new Spell(options.spellName)
    this.target = new Target(options.target)
  }

  get magicSchoolForSpell(): MagicSchool {
    return this.spell.magicSchool
  }

  get hasTrinkets(): boolean {
    // check trinkets only for now
    const trinket1 = this.character.equipment.items.trinket
    const trinket2 = this.character.equipment.items.trinket2

    return trinket1 !== undefined || trinket2 !== undefined
  }

  get hasHaste(): boolean {
    return this.character.getEffectiveSpellHaste() > 0
  }

  get numTicks() {
    return Math.floor(this.options.encLen / 2)
  }

  get manaCostPerCastWithoutClearCasting(): number {
    const baseManaCost = this.spell.manaCost
    switch (this.spell.magicSchool) {
      case MagicSchool.Fire: {
        const chanceToCrit = this.percentChanceToCrit()
        const critManaCost = baseManaCost * (1 - this.character.masterOfElementsManaCostReductionPercent)
        return chanceToCrit * critManaCost + (1 - chanceToCrit) * baseManaCost
      }
      case MagicSchool.Frost:
        return baseManaCost * (1 - this.character.frostChannelingManaCostReductionPercent)
      default:
        return baseManaCost
    }
  }

  get manaCostPerCast(): number {
    return this.manaCostPerCastWithoutClearCasting * (1 - this.character.arcaneConcentrationPercentChance)
  }

  get manaData(): ManaData {
    const manaGemAndTeaCD = 120
    const potionCD = 120
    const evocateCD = 488 // include use time in the cd
    const maxMana = this.character.mana

    const manaGemMana = 1100
    const manaPotionMana = 1800
    const teaMana = 1400
    const evocateMana = this.character.manaPerEvocate

    const clearCastingManaReduction = Math.round(
      this.manaCostPerCastWithoutClearCasting * this.character.arcaneConcentrationPercentChance
    )

    let currentPotionCD = 0
    let currentGemAndTeaCD = 0
    let currentEvocateCD = 0

    let currentMana = this.character.mana
    let currentCast = 0

    let manaGemsUsed = 0
    let manaPotionsUsed = 0
    let teasUsed = 0
    let evocatesUsed = 0

    let clearCastingManaSaved = 0

    let wentOOM = false

    for (let i = 0; i < this.options.encLen; i++) {
      const missingMana = maxMana - currentMana

      // check if we need to use mana gem / tea
      if (this.options.useMg && missingMana >= manaGemMana && currentGemAndTeaCD <= 0 && manaGemsUsed === 0) {
        manaGemsUsed++
        currentMana += manaGemMana
        currentGemAndTeaCD = manaGemAndTeaCD
      } else if (this.options.useTea && missingMana >= teaMana && currentGemAndTeaCD <= 0) {
        teasUsed++
        currentMana += teaMana
        currentGemAndTeaCD = manaGemAndTeaCD
      }

      // check if we need to use mana potion
      if (this.options.useMp && missingMana >= manaPotionMana && currentPotionCD <= 0) {
        manaPotionsUsed++
        currentMana += manaPotionMana
        currentPotionCD = potionCD
      }

      // check if we need to use evocate
      if (this.options.useEvo && missingMana >= evocateMana && currentEvocateCD <= 0) {
        evocatesUsed++
        currentMana += evocateMana
        currentEvocateCD = evocateCD
      }

      const numTheoreticalCasts = Math.floor(i / this.effectiveCastTime())
      if (numTheoreticalCasts > currentCast) {
        if (currentMana < 0) {
          wentOOM = true
        }

        // new cast occurred
        currentCast = numTheoreticalCasts
        clearCastingManaSaved += clearCastingManaReduction
        currentMana -= this.manaCostPerCast
      }

      // lower all CDs
      currentGemAndTeaCD--
      currentPotionCD--
      currentEvocateCD--
    }

    return {
      manaGemsUsed: manaGemsUsed,
      manaGemsRestored: manaGemsUsed * manaGemMana,
      manaPotionsUsed: manaPotionsUsed,
      manaPotionsRestored: manaPotionsUsed * manaPotionMana,
      teasUsed: teasUsed,
      teasRestored: teasUsed * teaMana,
      evocatesUsed: evocatesUsed,
      evocatesRestored: evocatesUsed * this.character.manaPerEvocate,
      clearCastingManaSaved: clearCastingManaSaved,
      finalMana: currentMana,
      wentOOM: wentOOM
    }
  }

  get curseOfShadowDamageBonus(): number {
    return this.spell.isArcane ? this.target.curseOfShadowDamageBonus : 1.0
  }

  get curseOfShadowResistBonus(): number {
    return this.spell.isArcane ? this.target.curseOfShadowResistBonus : 0
  }

  get curseOfElementsResistBonus(): number {
    return this.spell.isFire || this.spell.isFrost ? this.target.curseOfElementsResistBonus : 0
  }

  get curseOfElementsDamageBonus(): number {
    return this.spell.isFire || this.spell.isFrost ? this.target.curseOfElementsDamageBonus : 1.0
  }

  /**
   * Effect #1	Apply Aura: Mod % Damage Taken (All)
   * Value: -75%
   * Effect #2	Apply Aura: Mod % Damage Taken (Vulnerable)
   * Value: 1100%
   *
   */
  get shimmerBonus(): number {
    const modifier = this.target.shimmer > 0 ? 1 - 0.75 : 1
    return this.target.shimmer === this.spell.magicSchool ? modifier * (1 + 11) : modifier
  }

  get buffSpellPower(): number {
    return (
      this.character.buffs.flaskOfSupremePowerBonus +
      this.character.buffs.telAbimDelightSPBonus +
      this.character.buffs.dreamshardElixirSPBonus +
      this.character.buffs.dreamtonicSPBonus +
      this.character.buffs.greaterArcaneElixirBonus +
      this.character.buffs.brilliantWizardOilSpellDamageBonus +
      this.character.buffs.warlockAtieshSpellDamageBonus +
      this.character.buffs.blessedWizardOilSpellDamageBonus(this.target.options.type) +
      this.character.buffs.getElixirOfFrostPowerDamageBonus(this.spell.magicSchool) +
      this.character.buffs.getElixirOfGreaterFirePowerDamageBonus(this.spell.magicSchool)
    )
  }

  get baseSpellPower(): number {
    switch (this.spell.magicSchool) {
      case MagicSchool.Physical:
        return 0
      case MagicSchool.Arcane:
        return this.character.arcaneSpellPower + this.character.spellPower
      case MagicSchool.Nature:
        return this.character.natureSpellPower + this.character.spellPower
      case MagicSchool.Fire:
        return this.character.fireSpellPower + this.character.spellPower
      case MagicSchool.Frost:
        return this.character.frostSpellPower + this.character.spellPower
      case MagicSchool.Shadow:
      case MagicSchool.Holy:
      default:
        return this.character.spellPower
    }
  }

  get effectiveSpellCrit(): number {
    return Math.min(
      this.character.getEffectiveSpellCrit(this.magicSchoolForSpell, this.spell.name) +
        this.target.getWintersChillCritChanceBonus(this.magicSchoolForSpell),
      SPELL_CRIT_CAP
    )
  }

  get effectiveSpellPower(): number {
    return this.baseSpellPower + this.buffSpellPower
  }

  get effectiveTargetResistance(): number {
    const resistance = Math.min(this.target.spellResistance, 5 * this.character.level - this.targetResistanceFromLevel)
    return resistance - Math.min(this.spellPenetration, resistance) + this.targetResistanceFromLevel
  }

  /* For non-binary spells only: Each difference in level gives a 2% resistance chance that cannot
   * be negated (by spell penetration or otherwise). */
  get targetResistanceFromLevel(): number {
    if (this.spell.isBinary) {
      return 0
    }
    return (
      (this.target.level > this.character.level ? this.target.level - this.character.level : 0) *
      parseFloat((0.1333 * this.character.level).toFixed(2))
    )
  }

  /* https://dwarfpriest.wordpress.com/2008/01/07/spell-hit-spell-penetration-and-resistances/#more-176 */
  get partialResistPenalty(): number {
    return this.spell.canPartialResist ? (0.75 * this.effectiveTargetResistance) / (5 * this.character.level) : 0
  }

  get baseDmgMultiplier(): number {
    return 1
  }

  get effectiveDmgMultiplier(): number {
    return (
      this.character.buffs.powerInfusionDmgPercentBonus *
      this.character.buffs.saygesDarkFortuneBonus *
      this.character.buffs.tracesOfSilithystBonus *
      this.target.spellVulnerabilityBonus *
      this.target.getScorchVulnerabilityFireDmgBonus(this.spell.magicSchool) *
      this.character.getFirePowerDmgBonus(this.spell.magicSchool) *
      this.curseOfShadowDamageBonus *
      this.curseOfElementsDamageBonus *
      this.character.buffs.burningAdrenalineDamageBonus *
      this.character.buffs.thaddiusChargesDamageBonus *
      this.shimmerBonus *
      (1 - this.partialResistPenalty)
    )
  }

  get igniteDmgMultiplier(): number {
    return (
      this.character.buffs.powerInfusionDmgPercentBonus *
      this.target.spellVulnerabilityBonus *
      this.target.getScorchVulnerabilityFireDmgBonus(this.spell.magicSchool) *
      this.curseOfElementsDamageBonus *
      this.shimmerBonus
    )
  }

  /**
   * Mitigates spell resist of SpellCast. Needs work.
   */
  get spellPenetration(): number {
    switch (this.spell.magicSchool) {
      case MagicSchool.Arcane:
      case MagicSchool.Shadow:
        return this.character.equipment.spellPenetration + this.target.curseOfShadowResistBonus
      case MagicSchool.Nature:
        return this.character.equipment.spellPenetration
      case MagicSchool.Fire:
      case MagicSchool.Frost:
        return this.character.equipment.spellPenetration + this.target.curseOfElementsResistBonus
      default:
        return this.character.equipment.spellPenetration
    }
  }

  /**
   * Spell cast time . Factors in talents that modify base spell cast time.
   */
  get castTime(): number {
    let x

    switch (this.spell.baseName.toUpperCase()) {
      case 'FROSTBOLT':
        x = this.spell.castTime - this.character.improvedFrostBoltCastTimeReduction
        break
      case 'FIREBALL':
        x = this.spell.castTime - this.character.improvedFireballCastTimeReduction
        break
      default:
        x = this.spell.castTime <= GLOBAL_COOLDOWN ? GLOBAL_COOLDOWN : this.spell.castTime
        break
    }

    return x + this.options.castLag
  }

  /**
   * Factors in cast speed, procs like natures grace, hit, crit and "human factor" (which might actually be latency?)
   */
  get effectiveCastTimeWithoutHaste(): number {
    if (this.character.buffs.hasBurningAdrenaline) {
      return GLOBAL_COOLDOWN + this.options.castLag
    }

    return Math.max(GLOBAL_COOLDOWN + this.options.castLag, this.castTime)
  }

  effectiveCastTime(spellHaste = this.character.getEffectiveSpellHaste()): number {
    // arcane missiles doesn't get haste
    if (this.spell.name.includes('Arcane Missiles') || spellHaste === 0) {
      return this.effectiveCastTimeWithoutHaste
    }

    const castTimeWithoutLag = this.effectiveCastTimeWithoutHaste - this.options.castLag

    return (
      Math.max(GLOBAL_COOLDOWN + this.options.castLag, castTimeWithoutLag / (1 + spellHaste / 100)) +
      this.options.castLag
    )
  }

  get quicknessEffectiveCastTime(): number {
    // arcane missiles doesn't get haste
    if (this.spell.name.includes('Arcane Missiles')) {
      return this.effectiveCastTimeWithoutHaste
    }

    const spellHaste = this.character.getEffectiveSpellHaste() + 5 // 5% from quickness
    const castTimeWithoutLag = this.effectiveCastTimeWithoutHaste - this.options.castLag

    return (
      Math.max(GLOBAL_COOLDOWN + this.options.castLag, castTimeWithoutLag / (1 + spellHaste / 100)) +
      this.options.castLag
    )
  }

  /**
   * Chance of hitting with a spell
   *
   */
  chanceToHit(bonusHit = 0): number {
    return Math.min(
      99,
      this.target.hitChance + this.character.getEffectiveSpellHit(this.magicSchoolForSpell) + bonusHit
    )
  }

  /**
   * Chance of missing a spell
   *
   */
  get chanceToMiss(): number {
    return Math.max(1, 100 - this.chanceToHit())
  }

  /**
   * Chance of critting with a spell
   *
   */
  chanceToCrit(bonusHit = 0, bonusCrit = 0): number {
    const critChance =
      this.character.getEffectiveSpellCrit(this.magicSchoolForSpell, this.spell.name) +
      this.target.getWintersChillCritChanceBonus(this.magicSchoolForSpell) +
      bonusCrit
    return critChance * this.percentChanceToHit(bonusHit)
  }

  /**
   * Chance of landing a Normal spell hit i.e. not a miss and not a crit
   *
   */
  chanceToNormal(bonusHit = 0, bonusCrit = 0): number {
    return this.chanceToHit(bonusHit) - this.chanceToCrit(bonusHit, bonusCrit)
  }

  percentChanceToHit(bonusHit = 0): number {
    return this.chanceToHit(bonusHit) / 100
  }

  percentChanceToCrit(bonusHit = 0, bonusCrit = 0): number {
    return this.chanceToCrit(bonusHit, bonusCrit) / 100
  }

  percentChanceToNormal(bonusHit = 0, bonusCrit = 0): number {
    return this.chanceToNormal(bonusHit, bonusCrit) / 100
  }

  get spellSchoolCritMultiplier(): number {
    switch (this.magicSchoolForSpell) {
      case MagicSchool.Arcane:
        return this.character.arcaneCritMultiplier
      case MagicSchool.Fire:
        return 0
      case MagicSchool.Frost:
        return this.character.frostCritMultiplier
      default:
        return 0
    }
  }

  get critMultiplier(): number {
    return BASE_SPELL_CRIT_MULTIPLIER + this.spellSchoolCritMultiplier
  }

  /**
   * The bonus multiplier of a crit, not counting the base
   */
  get critBonusMultiplier(): number {
    return this.critMultiplier - 1
  }
}
