import Buffs from '../enum/Buffs'
import MagicSchool from '../enum/MagicSchool'
import OptionsCharacter from '../interface/OptionsCharacter'
import TargetType from '@/wow/enum/TargetType'

export default class Character {
  options: OptionsCharacter

  constructor(options: OptionsCharacter) {
    this.options = options
  }

  hasBuff(buff: Buffs): boolean {
    return this.options.buffs.includes(buff)
  }

  get moonkinAuraBonus(): number {
    if (this.hasBuff(Buffs.MoonkinAura)) {
      return this.hasBuff(Buffs.MoonkinAuraMoonclaw) ? 4 : 3
    }
    return 0
  }

  /* CONSUMABLE BUFFS */

  get flaskOfSupremePowerBonus(): number {
    return this.hasBuff(Buffs.FlaskOfSupremePower) ? 150 : 0
  }

  get greaterArcaneElixirBonus(): number {
    return this.hasBuff(Buffs.GreaterArcaneElixir) ? 35 : 0
  }

  get dreamshardElixirSPBonus(): number {
    return this.hasBuff(Buffs.DreamshardElixir) ? 15 : 0
  }

  get dreamtonicSPBonus(): number {
    return this.hasBuff(Buffs.Dreamtonic) ? 35 : 0
  }

  get dreamshardElixirCritBonus(): number {
    return this.hasBuff(Buffs.DreamshardElixir) ? 2 : 0
  }

  get cerebralCortexCompoundIntBonus(): number {
    return this.hasBuff(Buffs.CerebralCortexCompound) ? 25 : 0
  }

  get medivhsMerlotBlueIntBonus(): number {
    return this.hasBuff(Buffs.MedivhsMerlotBlue) ? 15 : 0
  }

  get telAbimDelightSPBonus(): number {
    return this.hasBuff(Buffs.TelAbimDelight) ? 22 : 0
  }

  get telAbimMedleyHasteBonus(): number {
    return this.hasBuff(Buffs.TelAbimMedley) ? 2 : 0
  }

  get runnTumTuberSurpriseIntBonus(): number {
    return this.hasBuff(Buffs.RunnTumTuberSurprise) ? 10 : 0
  }

  get mageAtieshSpellCritBonus(): number {
    return this.hasBuff(Buffs.AtieshAuraMage) ? 2 : 0
  }

  get brilliantWizardOilSpellCritBonus(): number {
    return this.hasBuff(Buffs.BrilliantWizardOil) ? 1 : 0
  }

  get warlockAtieshSpellDamageBonus(): number {
    return this.hasBuff(Buffs.AtieshAuraWarlock) ? 33 : 0
  }

  get brilliantWizardOilSpellDamageBonus(): number {
    return this.hasBuff(Buffs.BrilliantWizardOil) ? 36 : 0
  }

  blessedWizardOilSpellDamageBonus(targetType: TargetType): number {
    return this.hasBuff(Buffs.BlessedWizardOil) && targetType === TargetType.Undead ? 60 : 0
  }

  getElixirOfFrostPowerDamageBonus(magicSchool: MagicSchool): number {
    return this.hasBuff(Buffs.ElixirOfFrostPower) && magicSchool === MagicSchool.Frost ? 15 : 0
  }

  getElixirOfGreaterFirePowerDamageBonus(magicSchool: MagicSchool): number {
    return this.hasBuff(Buffs.ElixirOfGreaterFirePower) && magicSchool === MagicSchool.Fire ? 40 : 0
  }

  get powerInfusionDmgPercentBonus(): number {
    return this.hasBuff(Buffs.PowerInfusion) ? 1.2 : 1.0
  }

  get graceOfTheSunwellHasteBonus(): number {
    return this.hasBuff(Buffs.GraceOfTheSunwell) ? 10 : 0
  }

  /* WORLD BUFFS */

  get rallyingCryOfTheDragonSlayerSpellCritBonus(): number {
    return this.hasBuff(Buffs.RallyingCryOfTheDragonSlayer) ? 10 : 0
  }

  get slipkiksSavvyCritChanceBonus(): number {
    return this.hasBuff(Buffs.SlipkiksSavvy) ? 3 : 0
  }

  get songflowerSerenadeSpellCritBonus(): number {
    return this.hasBuff(Buffs.SongflowerSerenade) ? 5 : 0
  }

  get songflowerSerenadeAttributeBonus(): number {
    return this.hasBuff(Buffs.SongflowerSerenade) ? 15 : 0
  }

  get saygesDarkFortuneBonus(): number {
    return this.hasBuff(Buffs.SaygesDarkFortune) ? 1.1 : 1.0
  }

  get tracesOfSilithystBonus(): number {
    return this.hasBuff(Buffs.TracesOfSilithyst) ? 1.05 : 1.0
  }

  get spiritOfZandalarBonus(): number {
    return this.hasBuff(Buffs.SpiritOfZandalar) ? 1.15 : 1.0
  }

  /* RAID BUFFS */

  get emeraldBlessingHitBonus(): number {
    return this.hasBuff(Buffs.EmeraldBlessing) ? 1 : 0
  }

  get emeraldBlessingRegenWhileCastingPercent(): number {
    return this.hasBuff(Buffs.EmeraldBlessing) ? 5 : 0
  }

  get mageArmorRegenWhileCastingPercent(): number {
    return this.hasBuff(Buffs.MageArmor) ? 30 : 0
  }

  get arcaneBrillianceIntBonus(): number {
    return this.hasBuff(Buffs.ArcaneBrilliance) ? 31 : 0
  }

  get blessingOfKingsStatsBonus(): number {
    return this.hasBuff(Buffs.BlessingOfKings) ? 1.1 : 1.0
  }

  get blessingOfWisdomMp5Bonus(): number {
    return this.hasBuff(Buffs.BlessingOfWisdom) ? 33 : 0
  }

  get manaSpringTotemMp5Bonus(): number {
    return this.hasBuff(Buffs.ManaSpringTotem) ? 25 : 0
  }

  get druidAtieshMp5Bonus(): number {
    return this.hasBuff(Buffs.AtieshAuraDruid) ? 11 : 0
  }

  get GiftOfTheWildRk2AttributeBonus(): number {
    return this.hasBuff(Buffs.GiftOfTheWildRk2) ? 16 : 0
  }

  get GiftOfTheWildRk2ArmorBonus(): number {
    return this.hasBuff(Buffs.GiftOfTheWildRk2) ? 384 : 0
  }

  get GiftOfTheWildRk2ResistancesBonus(): number {
    return this.hasBuff(Buffs.GiftOfTheWildRk2) ? 27 : 0
  }

  get hasBurningAdrenaline(): boolean {
    return this.hasBuff(Buffs.BurningAdrenaline)
  }

  get burningAdrenalineDamageBonus(): number {
    return this.hasBuff(Buffs.BurningAdrenaline) ? 2 : 1
  }

  get hasThaddiusCharges(): boolean {
    return this.hasBuff(Buffs.ThaddiusCharges)
  }

  get thaddiusChargesDamageBonus(): number {
    return this.hasBuff(Buffs.ThaddiusCharges) ? 2.9 : 1
  }
}
