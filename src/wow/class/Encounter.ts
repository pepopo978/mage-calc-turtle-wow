import OptionsInterface from '../interface/Options'
import Cast from './Cast'
import DamageData from '@/wow/class/DamageData'

/* Encounter is the big top level object for all wow calculations. We want it run exactly once
   whenever a value in Options is changed.

   - Creates the Cast() object where most work is done
   - Generates the item and enchant list when clicking an item/enchant
   - Does the expensive gear optimization
*/
export default class Encounter {
  options: OptionsInterface
  spellCast: Cast
  damage: DamageData

  constructor(options: OptionsInterface) {
    this.options = options
    this.spellCast = new Cast(options)
    this.damage = new DamageData(this.spellCast)
  }

  reload() {
    this.spellCast = new Cast(this.options, this.equipment)
    this.damage = new DamageData(this.spellCast)
  }

  get equipment() {
    return this.spellCast.character.equipment
  }
}
