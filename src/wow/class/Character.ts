import Equipment from './Equipment'

import PlayableRace from '../enum/PlayableRace'
import Faction from '../enum/Faction'
import OptionsCharacter from '../interface/OptionsCharacter'
import MagicSchool from '../enum/MagicSchool'
import {
  BASE_SPELL_CRIT_MAGE,
  INT_FOR_1_SPELL_CRIT_MAGE,
  SPELL_CRIT_CAP,
  SPELL_HASTE_CAP,
  SPELL_HIT_CAP
} from '../module/Constants'
import CharacterBuffs from './CharacterBuffs'

/**
 * Stores character attributes, Talents, Gear, and Buffs
 */
export default class Character {
  options: OptionsCharacter
  equipment: Equipment
  buffs: CharacterBuffs

  constructor(options: OptionsCharacter, equipment: Equipment) {
    this.options = options
    this.equipment = equipment
    this.buffs = new CharacterBuffs(options)
  }

  get level(): number {
    return this.options.level
  }

  get faction(): Faction {
    return Character.factionFromRace(this.options.race)
  }

  get isHorde(): boolean {
    return (this.faction & Faction.Horde) === Faction.Horde
  }

  get isAlliance(): boolean {
    return (this.faction & Faction.Alliance) === Faction.Alliance
  }

  /**
   * TODO: https://classicwow.live/guides/46/basic-stats-sheet
   */

  get health(): number {
    return 10 * this.stamina + this.equipment.health
  }

  get mana(): number {
    return 964 + 15 * this.intellect
  }

  get raceStamina(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 25 // not sure if this is accurate
      case PlayableRace.Orc:
        return 23 + 25 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 25 + 25 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 20 + 25 // not sure if this is accurate
      case PlayableRace.Undead:
        return 21 + 25 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 25 + 25 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 19 + 25 // not sure if this is accurate
      case PlayableRace.Troll:
        return 22 + 25 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 22 + 25 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 19 + 25 // not sure if this is accurate
      default:
        return 0
    }
  }

  get armor(): number {
    return this.equipment.armor // TODO add armor from buffs
  }

  get stamina(): number {
    return (
      (this.raceStamina + this.equipment.stamina + this.buffs.GiftOfTheWildRk2AttributeBonus) *
      this.buffs.spiritOfZandalarBonus *
      this.buffs.blessingOfKingsStatsBonus
    )
  }

  get raceIntellect(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 118 // not sure if this is accurate
      case PlayableRace.Orc:
        return 17 + 118 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 19 + 118 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 20 + 118 // not sure if this is accurate
      case PlayableRace.Undead:
        return 18 + 118 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 23 + 118 // not sure if this is accurate
      case PlayableRace.Troll:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 23 + 118 // not sure if this is accurate
      default:
        return 0
    }
  }

  get intellect(): number {
    const gnomeMultiplier = this.options.race === PlayableRace.Gnome ? 1.05 : 1
    return (
      (this.raceIntellect +
        this.equipment.intellect +
        this.buffs.arcaneBrillianceIntBonus +
        this.buffs.GiftOfTheWildRk2AttributeBonus +
        this.buffs.songflowerSerenadeAttributeBonus +
        this.buffs.cerebralCortexCompoundIntBonus +
        this.buffs.medivhsMerlotBlueIntBonus +
        this.buffs.runnTumTuberSurpriseIntBonus) *
      this.buffs.spiritOfZandalarBonus *
      this.buffs.blessingOfKingsStatsBonus *
      gnomeMultiplier
    )
  }

  get raceSpirit(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.Orc:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 25 + 100 // not sure if this is accurate
      case PlayableRace.Undead:
        return 23 + 100 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.Troll:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 20 + 100 // not sure if this is accurate
      default:
        return 0
    }
  }

  get spirit(): number {
    return (
      this.raceSpirit +
      this.equipment.spirit +
      this.buffs.GiftOfTheWildRk2AttributeBonus *
        this.buffs.spiritOfZandalarBonus *
        this.buffs.blessingOfKingsStatsBonus
    )
  }

  get mp5(): number {
    return (
      this.equipment.mp5 +
      this.buffs.blessingOfWisdomMp5Bonus +
      this.buffs.manaSpringTotemMp5Bonus +
      this.buffs.druidAtieshMp5Bonus
    )
  }

  get hp5(): number {
    return this.equipment.hp5
  }

  get percentManaRegenWhileCasting(): number {
    return (
      this.equipment.percentManaRegenWhileCasting +
      this.arcaneMeditationPercentRegen +
      this.buffs.emeraldBlessingRegenWhileCastingPercent +
      this.buffs.mageArmorRegenWhileCastingPercent
    )
  }

  get fireResistance(): number {
    return this.equipment.fireResistance
  }

  get frostResistance(): number {
    return this.equipment.frostResistance
  }

  get arcaneResistance(): number {
    return this.equipment.arcaneResistance
  }

  get natureResistance(): number {
    return this.equipment.natureResistance
  }

  get shadowResistance(): number {
    return this.equipment.shadowResistance
  }

  get manaPerTickNotCasting(): number {
    const fromBase = (15 * this.level) / 60
    const fromSpirit = this.spirit / 5
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromBase + fromSpirit + fromMp5
  }

  get manaPerTickCastingNoMp5(): number {
    const fromBase = (15 * this.level) / 60
    const fromSpirit = this.spirit / 5

    return Math.round((this.percentManaRegenWhileCasting / 100) * (fromBase + fromSpirit))
  }

  get manaPerTickCasting(): number {
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromMp5 + this.manaPerTickCastingNoMp5
  }

  get manaPerTickInnervate(): number {
    return this.manaPerTickNotCasting * 4
  }

  get manaPerInnervate(): number {
    return this.manaPerTickInnervate * 10
  }

  get manaPerTickEvocate(): number {
    return Math.round(this.manaPerTickNotCasting * 15)
  }

  get manaPerEvocate(): number {
    return this.manaPerTickEvocate * 4
  }

  get spellPower(): number {
    return this.equipment.spellPower
  }

  get arcaneSpellPower(): number {
    return this.equipment.arcaneSpellPower
  }

  get frostSpellPower(): number {
    return this.equipment.frostSpellPower
  }

  get fireSpellPower(): number {
    return this.equipment.fireSpellPower
  }

  get natureSpellPower(): number {
    return this.equipment.natureSpellPower
  }

  get shadowDamage(): number {
    return this.equipment.shadowSpellPower
  }

  get spellCritFromIntellect(): number {
    return this.intellect / INT_FOR_1_SPELL_CRIT_MAGE
  }

  get spellCritFromEquipment(): number {
    return this.equipment.spellCrit
  }

  get spellHasteFromEquipment(): number {
    return this.equipment.spellHaste
  }

  get spellVampFromEquipment(): number {
    return this.equipment.spellVamp
  }

  get arcaneCritMultiplier(): number {
    return this.arcanePotencyCritBonus
  }

  get frostCritMultiplier(): number {
    return this.iceShardsCritBonus
  }

  // Reduces target resistance to all spells
  get arcaneSubtletyResistanceReduction(): number {
    switch (this.options.talents.arcane.arcaneSubtlety) {
      case 1:
        return 5
      case 2:
        return 10
      default:
        return 0
    }
  }

  // Improved hit for arcane spells
  get improvedFrostBoltCastTimeReduction(): number {
    return this.options.talents.frost.improvedFrostbolt * 0.1
  }

  get improvedFireballCastTimeReduction(): number {
    return this.options.talents.fire.improvedFireball * 0.1
  }

  // Improved hit for arcane spells
  get arcaneFocusHit(): number {
    return this.options.talents.arcane.arcaneFocus * 2
  }

  get acceleratedArcanaHaste(): number {
    return this.options.talents.arcane.acceleratedArcana * 5
  }

  // Improved hit for fire/frost spells
  get elementalPrecisionHit(): number {
    return this.options.talents.frost.elementalPrecision * 2
  }

  // Chance for clearcasting
  get arcaneConcentrationPercentChance(): number {
    return this.options.talents.arcane.arcaneConcentration * 0.02
  }

  get frostChannelingManaCostReductionPercent(): number {
    return this.options.talents.frost.frostChanneling * 0.05
  }

  get masterOfElementsManaCostReductionPercent(): number {
    return this.options.talents.fire.masterOfElements * 0.1
  }

  /**
   * Improves crit chance for arcane focus and arcance missiles
   */
  get arcaneImpactCritChance(): number {
    return this.options.talents.arcane.arcaneImpact * 2
  }

  /* TALENTS */

  /**
   * Improves crit chance for all fire spells
   */
  get criticalMassCritChance(): number {
    return this.options.talents.fire.criticalMass * 2
  }

  get incinerateCritChance(): number {
    return this.options.talents.fire.incinerate * 2
  }

  // Returns ignite dmg percent based on tooltip
  get igniteCritDmgPercent(): number {
    return this.options.talents.fire.ignite * 0.08
  }

  // Returns ignite dmg on turtle wow per tick
  get igniteCritDmgPercentPerTick(): number {
    return (this.options.talents.fire.ignite * 0.08) / 2
  }

  get arcanePotencyCritBonus(): number {
    switch (this.options.talents.arcane.arcanePotency) {
      case 1:
        return 0.09
      case 2:
        return 0.18
      case 3:
        return 0.25
      default:
        return 0
    }
  }

  get iceShardsCritBonus(): number {
    return this.options.talents.frost.iceShards * 0.1
  }

  /**
   * Allows % of mana regen to continue while casting
   */
  get arcaneMeditationPercentRegen(): number {
    return this.options.talents.arcane.arcaneMeditation * 5
  }

  static factionFromRace(race: PlayableRace): Faction {
    switch (race) {
      case PlayableRace.Tauren:
      case PlayableRace.Orc:
      case PlayableRace.Undead:
      case PlayableRace.Troll:
      case PlayableRace.Goblin:
        return Faction.Horde
      default:
        return Faction.Alliance
    }
  }

  getTalentSpellCrit(spellSchool: MagicSchool, spellName: string): number {
    switch (spellSchool) {
      case MagicSchool.Arcane:
        return this.arcaneImpactCritChance
      case MagicSchool.Fire: {
        if (spellName.includes('Scorch') || spellName.includes('Fire Blast')) {
          return this.criticalMassCritChance + this.incinerateCritChance
        }
        return this.criticalMassCritChance
      }
      case MagicSchool.Frost:
        return 0
      default:
        return 0
    }
  }

  getSpellCritUnbuffed(spellSchool: MagicSchool, spellName: string): number {
    return (
      BASE_SPELL_CRIT_MAGE +
      this.spellCritFromIntellect +
      this.spellCritFromEquipment +
      this.getTalentSpellCrit(spellSchool, spellName)
    )
  }

  getSpellCrit(spellSchool: MagicSchool, spellName: string): number {
    return (
      this.getSpellCritUnbuffed(spellSchool, spellName) +
      this.buffs.rallyingCryOfTheDragonSlayerSpellCritBonus +
      this.buffs.dreamshardElixirCritBonus +
      this.buffs.moonkinAuraBonus +
      this.buffs.slipkiksSavvyCritChanceBonus +
      this.buffs.songflowerSerenadeSpellCritBonus +
      this.buffs.brilliantWizardOilSpellCritBonus +
      this.buffs.mageAtieshSpellCritBonus
    )
  }

  getEffectiveSpellCrit(spellSchool: MagicSchool, spellName: string): number {
    return Math.min(this.getSpellCrit(spellSchool, spellName), SPELL_CRIT_CAP)
  }

  /**
   * TODO: Return total spell hit rating (equipment + talents + buffs)
   */
  getEffectiveSpellHit(spellSchool: MagicSchool): number {
    return Math.min(this.getSpellHit(spellSchool), SPELL_HIT_CAP)
  }

  getSpellHasteFromBuffs(): number {
    return this.buffs.telAbimMedleyHasteBonus
  }

  getEffectiveSpellHaste(): number {
    const gearSpellHaste = this.equipment.spellHaste

    const totalHasteScalingFactor =
      (1 + this.buffs.telAbimMedleyHasteBonus / 100) *
      (1 + this.acceleratedArcanaHaste / 100) *
      (1 + gearSpellHaste / 100)

    const totalHastePercentage = (totalHasteScalingFactor - 1) * 100

    return Math.min(Math.round(totalHastePercentage * 100) / 100, SPELL_HASTE_CAP)
  }

  getSpellHit(spellSchool: MagicSchool): number {
    let spellSchoolTalentHit = 0
    switch (spellSchool) {
      case MagicSchool.Arcane:
        spellSchoolTalentHit = this.arcaneFocusHit
        break
      case MagicSchool.Fire:
      case MagicSchool.Frost:
        spellSchoolTalentHit = this.elementalPrecisionHit
        break
    }

    return this.equipment.spellHit + this.buffs.emeraldBlessingHitBonus + spellSchoolTalentHit
  }

  getSpellVamp(_spellSchool: MagicSchool): number {
    return this.spellVampFromEquipment
  }

  getFirePowerDmgBonus(spellSchool: MagicSchool): number {
    return spellSchool === MagicSchool.Fire ? 1 + this.options.talents.fire.firePower * 0.02 : 1
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
