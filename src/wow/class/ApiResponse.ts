export interface ApiResponse {
  status: boolean
  result: ApiResult
}

export interface ApiResult {
  character: CharacterEntity[]
  items: ItemsEntity[]
  skills: SkillsEntity[]
  guild?: GuildEntity[] | null
  talents: number[]
  talent_link: string
  rep?: RepEntity[]
}

export interface CharacterEntity {
  guid: number
  name: string
  race: number
  class: number
  gender: number
  level: number
  honorRankPoints: number
  honorHighestRank: number
  playerBytes: number
  ignore_titles: number
  city_protector: number
  playerBytes2: number
  Rank: string
  online: number
  pb: Pb
  rank_number: string
}

// this is character appearance information like skin color, hair style, etc
export interface Pb {
  sk: number
  fa: number
  ha: number
  hc: number
  fh: number
  fc: number
  ep: number
  ho: number
  ta: number
}

export interface ItemsEntity {
  item_template: number
  slot: number
  bag: number
  class: number
  subclass: number
  name: string
  inventory_type: number
  enchantments: number
  displayId: number
  quality: number
  icon: string
}

export interface SkillsEntity {
  skill: number
  value: number
  max: number
}

export interface GuildEntity {
  name: string
}

export interface RepEntity {
  faction: number
  standing: number
}
