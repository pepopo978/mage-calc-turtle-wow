import EnchantJSON from '../interface/EnchantJSON'
import ItemJSON from '../interface/ItemJSON'

import ItemClass from '../enum/ItemClass'
import ItemQuality from '../enum/ItemQuality'
import ItemSlot from '../enum/ItemSlot'
import MagicSchool from '../enum/MagicSchool'
import PlayableClass from '../enum/PlayableClass'
import TargetType from '../enum/TargetType'
import { GLOBAL_COOLDOWN, INT_FOR_1_SPELL_CRIT_MAGE, UNEQUIPPED_ITEM_ID } from '../module/Constants'
import Equipment from './Equipment'
import ScoreInfo from '../interface/ScoreInfo'
import { trinketDmgValues } from './Trinkets'
import HasteInfo from '../interface/HasteInfo'

export default class Item {
  slot: ItemSlot
  itemJSON: ItemJSON
  enchantJSON: EnchantJSON | undefined
  locked: boolean = false
  enchantLocked: boolean = false

  constructor(slot: ItemSlot, itemJSON: ItemJSON, enchantJSON?: EnchantJSON) {
    this.slot = slot
    this.itemJSON = itemJSON
    this.enchantJSON = enchantJSON ? enchantJSON : undefined
  }

  static sortScoreAsc(a: ItemJSON | EnchantJSON, b: ItemJSON | EnchantJSON) {
    return (a.score ? a.score : 0) - (b.score ? b.score : 0)
  }

  static sortScoreDes(a: ItemJSON | EnchantJSON, b: ItemJSON | EnchantJSON) {
    return (b.score ? b.score : 0) - (a.score ? a.score : 0)
  }

  /* Handle items that only damage certain types of mobs */
  static calcTargetDamage(targetType: number, targetTypes: number, spellDamage: number): number {
    if (targetTypes === TargetType.All) {
      return spellDamage
    }

    switch (targetType) {
      case TargetType.Undead:
        return (targetTypes & TargetType.Undead) === TargetType.Undead ? spellDamage : 0
      case TargetType.Demon:
        return (targetTypes & TargetType.Demon) === TargetType.Demon ? spellDamage : 0
      default:
        return 0
    }
  }

  static scoreItem(itemJSON: ItemJSON, scoreInfo: ScoreInfo): number {
    let score = this.score(
      this.calcTargetDamage(scoreInfo.targetType, itemJSON.effects.targetTypes, itemJSON.effects.spellPower),
      itemJSON.effects.arcaneSpellPower,
      itemJSON.effects.natureSpellPower,
      itemJSON.effects.fireSpellPower,
      itemJSON.effects.frostSpellPower,
      itemJSON.effects.spellHit,
      itemJSON.effects.spellCrit,
      itemJSON.effects.spellPen,
      itemJSON.intellect,
      scoreInfo
    )

    if (itemJSON.effects.onUse && (itemJSON.slot === ItemSlot.Trinket || itemJSON.slot === ItemSlot.Trinket2)) {
      const values = trinketDmgValues(itemJSON, scoreInfo)
      if (values.onUse) {
        itemJSON.onUseScore = values.onUse.avgSPPerCast
        score += itemJSON.onUseScore
        score = Math.round(score * 100) / 100 // round to 2 decimal
      }
    }

    if (itemJSON.effects.spellHaste > 0) {
      const hasteScore = Item.calculateHasteScore(itemJSON.effects.spellHaste, scoreInfo)
      score += hasteScore
      score = Math.round(score * 100) / 100 // round to 2 decimal
    }

    return score
  }

  static scoreItemSetBonus(itemJSON: ItemJSON, equipment: Equipment, scoreInfo: ScoreInfo): number {
    const itemSetVirtualItems = equipment.getItemSetVirtualItems(itemJSON)
    let totalScore = 0
    itemSetVirtualItems.forEach(virtualItem => {
      const effects = virtualItem.itemJSON.effects
      totalScore += this.score(
        this.calcTargetDamage(
          scoreInfo.targetType,
          virtualItem.targetTypes ? virtualItem.targetTypes : TargetType.All,
          effects.spellPower
        ),
        effects.arcaneSpellPower,
        effects.natureSpellPower,
        effects.fireSpellPower,
        effects.frostSpellPower,
        effects.spellHit,
        effects.spellCrit,
        effects.spellPen,
        virtualItem.intellect,
        scoreInfo
      )
    })
    return totalScore
  }

  static scoreEnchant(enchant: EnchantJSON, scoreInfo: ScoreInfo): number {
    let score = this.score(
      enchant.effects.spellPower,
      enchant.effects.arcaneSpellPower,
      enchant.effects.natureSpellPower,
      enchant.effects.fireSpellPower,
      enchant.effects.frostSpellPower,
      enchant.effects.spellHit,
      enchant.effects.spellCrit,
      enchant.effects.spellPen,
      enchant.effects.intellect ? enchant.effects.intellect : 0,
      scoreInfo
    )

    if (enchant.effects.spellHaste > 0) {
      score += Item.calculateHasteScore(enchant.effects.spellHaste, scoreInfo)
      score = Math.round(score * 100) / 100 // round to 2 decimal
    }
    return score
  }

  static score(
    spellDamage: number,
    arcaneDamage: number,
    natureDamage: number,
    fireDamage: number,
    frostDamage: number,
    spellHit: number,
    spellCrit: number,
    spellPenetration: number,
    intellect: number,
    scoreInfo: ScoreInfo
  ): number {
    const magicSchool = scoreInfo.magicSchool
    const totalSpellDamage =
      spellDamage +
      (magicSchool && magicSchool === MagicSchool.Arcane ? arcaneDamage : 0) +
      (magicSchool && magicSchool === MagicSchool.Nature ? natureDamage : 0) +
      (magicSchool && magicSchool === MagicSchool.Fire ? fireDamage : 0) +
      (magicSchool && magicSchool === MagicSchool.Frost ? frostDamage : 0)
    const totalScore =
      totalSpellDamage +
      spellHit * scoreInfo.spellHitWeight +
      spellCrit * scoreInfo.spellCritWeight +
      spellPenetration * scoreInfo.spellPenetrationWeight +
      (intellect / INT_FOR_1_SPELL_CRIT_MAGE) * scoreInfo.spellCritWeight

    return parseFloat(totalScore.toFixed(2))
  }

  static calculateHasteScore(spellHaste: number, hasteInfo: HasteInfo): number {
    if (hasteInfo.spellName.includes('Arcane Missiles')) {
      return 0
    }

    const baseCastTimeWithoutLag = hasteInfo.spellBaseCastTime - hasteInfo.castLag

    // if faster than gcd, haste is capped
    if (baseCastTimeWithoutLag <= GLOBAL_COOLDOWN) {
      return 0
    }

    const hasteScalingFactor = (1 + hasteInfo.spellHaste / 100) * (1 + spellHaste / 100)

    const castTimeWithItem = baseCastTimeWithoutLag / hasteScalingFactor + hasteInfo.castLag
    let castsWithoutItem = hasteInfo.encLen / hasteInfo.spellEffectiveCastTime
    let castsWithItem = hasteInfo.encLen / castTimeWithItem

    if (!hasteInfo.partialCasts) {
      castsWithoutItem = Math.floor(castsWithoutItem)
      castsWithItem = Math.floor(castsWithItem)
    }

    // calculate additional casts
    const additionalCasts = castsWithItem - castsWithoutItem

    const totalDmg = Math.round(additionalCasts * hasteInfo.castDmgEV)
    const totalSP = totalDmg / hasteInfo.spellPowerToDmgRatio

    return totalSP / castsWithoutItem
  }

  updateScore(equipment: Equipment, scoreInfo: ScoreInfo) {
    this.itemJSON.setBonusScore = Item.scoreItemSetBonus(this.itemJSON, equipment, scoreInfo)
    this.itemJSON.score = Item.scoreItem(this.itemJSON, scoreInfo) + this.itemJSON.setBonusScore
    this.itemJSON.score = Math.round(this.itemJSON.score * 100) / 100
  }

  get id(): number {
    return this.itemJSON.id
  }

  get enchantId(): number {
    return this.enchantJSON && this.enchantJSON.id ? this.enchantJSON.id : 0
  }

  get name(): string {
    return this.itemJSON ? this.itemJSON.name : this.slot
  }

  get class(): ItemClass {
    switch (this.slot) {
      case ItemSlot.Twohand:
      case ItemSlot.Mainhand:
      case ItemSlot.Onehand:
        return ItemClass.Weapon
      default:
        return ItemClass.Armor
    }
  }

  get isWeapon(): boolean {
    return this.class === ItemClass.Weapon
  }

  get isArmor(): boolean {
    return this.class === ItemClass.Armor
  }

  matchesTargetType(targetType: TargetType): boolean {
    return this.itemJSON.effects.targetTypes === TargetType.All || (this.itemJSON.effects.targetTypes & targetType) > 0
  }

  get toolTipText(): string[] {
    const bonuses: string[] = []
    if (this._spellHit > 0) {
      bonuses.push(`Equip: Improves your chance to hit with spells by ${this._spellHit}%.`)
    }

    if (this._spellCrit > 0) {
      bonuses.push(`Equip: Improves your chance to get a critical strike with spells by ${this._spellCrit}%.`)
    }

    if (this._spellHaste > 0) {
      bonuses.push(`Equip: Increases your attack and casting speed by ${this._spellHaste}%.`)
    }

    if (this._spellVamp > 0) {
      bonuses.push(`Equip: ${this._spellVamp}% of damage dealt is returned as healing.`)
    }

    if (this._spellPower > 0) {
      if (
        (this.targetTypes & TargetType.Undead) === TargetType.Undead &&
        (this.targetTypes & TargetType.Demon) === TargetType.Demon
      ) {
        bonuses.push(
          `Equip: Increases damage done to Undead and Demons by magical spells and effects by up to ${this._spellPower}.`
        )
      } else if ((this.targetTypes & TargetType.Undead) === TargetType.Undead) {
        bonuses.push(
          `Equip: Increases damage done to Undead by magical spells and effects by up to ${this._spellPower}.`
        )
      } else {
        bonuses.push(
          `Equip: Increases damage and healing done by magical spells and effects by up to ${this._spellPower}.`
        )
      }
    }

    if (this._arcaneDamage > 0) {
      bonuses.push(`Equip: Increases damage done by Arcane spells and effects by up to ${this._arcaneDamage}.`)
    }

    if (this._natureDamage > 0) {
      bonuses.push(`Equip: Increases damage done by Nature spells and effects by up to ${this._natureDamage}.`)
    }

    if (this._fireDamage > 0) {
      bonuses.push(`Equip: Increases damage done by Fire spells and effects by up to ${this._fireDamage}.`)
    }

    if (this._frostDamage > 0) {
      bonuses.push(`Equip: Increases damage done by Frost spells and effects by up to ${this._frostDamage}.`)
    }

    if (this.spellPenetration > 0) {
      bonuses.push(`Equip: Decreases the magical resistances of your spell targets by ${this.spellPenetration}.`)
    }

    if (this._mp5 > 0) {
      bonuses.push(`Equip: Restores ${this._mp5} mana per 5 sec.`)
    }

    if (this.hp5 > 0) {
      bonuses.push(`Equip: Restores ${this.hp5} health per 5 sec.`)
    }

    return bonuses
  }

  get isUnequipped(): boolean {
    if (!this.itemJSON) {
      return true
    }

    if (this.itemJSON.id === UNEQUIPPED_ITEM_ID) {
      return true
    }

    return false
  }

  get quality(): ItemQuality {
    return this.itemJSON.quality
  }

  get qualityName(): string {
    return ItemQuality[this.quality]
  }

  get level(): number {
    return this.itemJSON.level
  }

  get isBop(): boolean {
    return !this.itemJSON.boe
  }

  get isUnique(): boolean {
    return this.itemJSON.unique
  }

  get isPartOfItemSet(): boolean {
    return this.itemJSON && this.itemJSON.setName !== ''
  }

  get allowableClasses(): PlayableClass[] {
    // if "all"
    if (this.itemJSON.allowableClasses.includes('all')) {
      return [
        PlayableClass.Warrior,
        PlayableClass.Paladin,
        PlayableClass.Hunter,
        PlayableClass.Rogue,
        PlayableClass.Priest,
        PlayableClass.Shaman,
        PlayableClass.Mage,
        PlayableClass.Warlock,
        PlayableClass.Druid
      ]
    }
    const allowedClassInts: Array<number> = []
    this.itemJSON.allowableClasses.forEach(allowedClass => {
      switch (allowedClass) {
        case 'warrior':
          allowedClassInts.push(PlayableClass.Warrior)
          break
        case 'paladin':
          allowedClassInts.push(PlayableClass.Paladin)
          break
        case 'hunter':
          allowedClassInts.push(PlayableClass.Hunter)
          break
        case 'rogue':
          allowedClassInts.push(PlayableClass.Rogue)
          break
        case 'priest':
          allowedClassInts.push(PlayableClass.Priest)
          break
        case 'shaman':
          allowedClassInts.push(PlayableClass.Shaman)
          break
        case 'mage':
          allowedClassInts.push(PlayableClass.Mage)
          break
        case 'warlock':
          allowedClassInts.push(PlayableClass.Warlock)
          break
        case 'druid':
          allowedClassInts.push(PlayableClass.Druid)
          break
        default:
          console.warn(`Unknown class: ${allowedClass}`)
          break
      }
    })
    return allowedClassInts
  }

  get allowableClassesText(): string {
    return this.itemJSON.allowableClasses.join(', ')
  }

  get targetTypes(): TargetType {
    return this.itemJSON.effects.targetTypes
  }

  get icon(): string {
    let emptySlot = this.slot
    if (emptySlot === ItemSlot.Onehand) {
      emptySlot = ItemSlot.Mainhand
    }

    return this.isUnequipped || !this.itemJSON ? `${emptySlot}.jpg` : `${this.itemJSON.icon}`
  }

  get iconFullPath(): string {
    return process.env.BASE_URL + 'wow-icons/' + this.icon
  }

  get customEffects(): string {
    return this.itemJSON.effects.custom.join(', ')
  }

  get bindText(): string {
    return 'Binds ' + (this.isBop ? 'when picked up' : 'when equipped')
  }

  get _stamina(): number {
    return this.itemJSON.stamina
  }

  get stamina(): number {
    return (
      this._stamina + (this.enchantJSON ? (this.enchantJSON.effects.stamina ? this.enchantJSON.effects.stamina : 0) : 0)
    )
  }

  get _health(): number {
    return this.itemJSON.effects.hp ? this.itemJSON.effects.hp : 0
  }

  get health(): number {
    return this._health + (this.enchantJSON ? (this.enchantJSON.effects.hp ? this.enchantJSON.effects.hp : 0) : 0)
  }

  get _spirit(): number {
    return this.itemJSON.spirit
  }

  get spirit(): number {
    return (
      this._spirit + (this.enchantJSON ? (this.enchantJSON.effects.spirit ? this.enchantJSON.effects.spirit : 0) : 0)
    )
  }

  get _intellect(): number {
    return this.itemJSON.intellect
  }

  get intellect(): number {
    return (
      this._intellect +
      (this.enchantJSON ? (this.enchantJSON.effects.intellect ? this.enchantJSON.effects.intellect : 0) : 0)
    )
  }

  get _spellHealing(): number {
    return this.itemJSON.effects.healPower
  }

  get spellHealing(): number {
    return this._spellHealing + (this.enchantJSON ? this.enchantJSON.effects.healPower : 0)
  }

  get _spellPower(): number {
    return this.itemJSON.effects.spellPower
  }

  get spellPower(): number {
    return this._spellPower + (this.enchantJSON ? this.enchantJSON.effects.spellPower : 0)
  }

  getSpellPowerForSchool(magicSchool: MagicSchool): number {
    switch (magicSchool) {
      case MagicSchool.Arcane:
        return this._arcaneDamage + this.spellPower
      case MagicSchool.Nature:
        return this._natureDamage + this.spellPower
      case MagicSchool.Fire:
        return this._fireDamage + this.spellPower
      case MagicSchool.Frost:
        return this._frostDamage + this.spellPower
      case MagicSchool.Shadow:
        return this._shadowDamage + this.spellPower
      default:
        return this.spellPower
    }
  }

  get spellPenetration(): number {
    return this.itemJSON.effects.spellPen
  }

  get _arcaneDamage(): number {
    return this.itemJSON.effects.arcaneSpellPower
  }

  get arcaneDamage(): number {
    return this._arcaneDamage + (this.enchantJSON ? this.enchantJSON.effects.arcaneSpellPower : 0)
  }

  get _fireDamage(): number {
    return this.itemJSON.effects.fireSpellPower
  }

  get fireDamage(): number {
    return this._fireDamage + (this.enchantJSON ? this.enchantJSON.effects.fireSpellPower : 0)
  }

  get _frostDamage(): number {
    return this.itemJSON.effects.frostSpellPower
  }

  get frostDamage(): number {
    return this._frostDamage + (this.enchantJSON ? this.enchantJSON.effects.frostSpellPower : 0)
  }

  get _natureDamage(): number {
    return this.itemJSON.effects.natureSpellPower
  }

  get natureDamage(): number {
    return this._natureDamage + (this.enchantJSON ? this.enchantJSON.effects.natureSpellPower : 0)
  }

  get _shadowDamage(): number {
    return this.itemJSON.effects.shadowSpellPower
  }

  get shadowDamage(): number {
    return this._shadowDamage + (this.enchantJSON ? this.enchantJSON.effects.shadowSpellPower : 0)
  }

  get _spellHit(): number {
    return this.itemJSON.effects.spellHit
  }

  get spellHit(): number {
    return this._spellHit + (this.enchantJSON ? this.enchantJSON.effects.spellHit : 0)
  }

  get _spellCrit(): number {
    return this.itemJSON.effects.spellCrit
  }

  get spellCrit(): number {
    return this._spellCrit + (this.enchantJSON ? this.enchantJSON.effects.spellCrit : 0)
  }

  get _spellHaste(): number {
    return this.itemJSON.effects.spellHaste
  }

  get spellHaste(): number {
    return this._spellHaste + (this.enchantJSON ? this.enchantJSON.effects.spellHaste : 0)
  }

  get _spellVamp(): number {
    return this.itemJSON.effects.spellVamp
  }

  get spellVamp(): number {
    return this._spellVamp + (this.enchantJSON ? this.enchantJSON.effects.spellVamp : 0)
  }

  get _mp5(): number {
    return this.itemJSON.effects.mp5
  }

  get mp5(): number {
    return this._mp5 + (this.enchantJSON ? this.enchantJSON.effects.mp5 : 0)
  }

  get hp5(): number {
    return this.itemJSON.effects.hp5
  }

  get _armor(): number {
    return this.itemJSON.armor
  }

  get armor(): number {
    return this._armor + (this.enchantJSON ? (this.enchantJSON.effects.armor ? 0 : 0) : 0)
  }

  get durability(): number {
    return this.itemJSON.durability
  }

  get minDmg(): number {
    return this.itemJSON.weaponStats ? this.itemJSON.weaponStats.minDmg : 0
  }

  get maxDmg(): number {
    return this.itemJSON.weaponStats ? this.itemJSON.weaponStats.maxDmg : 0
  }

  get dmgText(): string {
    return `${this.minDmg} - ${this.maxDmg}`
  }

  get speed(): number {
    return this.itemJSON.weaponStats ? this.itemJSON.weaponStats.speed : 0
  }

  get speedText(): string {
    if (!this.speed) {
      return ''
    }
    return `${parseFloat(this.speed.toFixed(1)).toFixed(2)}`
  }

  get dps(): number {
    return this.itemJSON.weaponStats ? this.itemJSON.weaponStats.dps : 0
  }

  get dpsText(): string {
    if (!this.dps) {
      return ''
    }
    return `${parseFloat(this.dps.toFixed(1)).toFixed(2)}`
  }

  get hasEnchant(): boolean {
    switch (this.slot) {
      case ItemSlot.Head:
      case ItemSlot.Hands:
      case ItemSlot.Shoulder:
      case ItemSlot.Waist:
      case ItemSlot.Legs:
      case ItemSlot.Back:
      case ItemSlot.Feet:
      case ItemSlot.Chest:
      case ItemSlot.Wrist:
      case ItemSlot.Twohand:
      case ItemSlot.Onehand:
      case ItemSlot.Mainhand:
        return true
      default:
        return false
    }
  }

  get enchantText(): string {
    const text = this.enchantJSON ? this.enchantJSON.text : 'No Enchant'

    switch (this.slot) {
      case ItemSlot.Head:
      case ItemSlot.Hands:
      case ItemSlot.Shoulder:
      case ItemSlot.Waist:
      case ItemSlot.Legs:
      case ItemSlot.Back:
      case ItemSlot.Feet:
      case ItemSlot.Chest:
      case ItemSlot.Wrist:
      case ItemSlot.Twohand:
      case ItemSlot.Onehand:
      case ItemSlot.Mainhand:
        return text
      default:
        return ``
    }
  }

  get enchantClass(): string {
    if (!this.enchantJSON) {
      return `poor` // TODO fix
    }

    const slot = this.enchantJSON.slot

    if (this.enchantJSON.id === 1) {
      return `poor`
    }

    switch (slot) {
      case ItemSlot.Head:
      case ItemSlot.Hands:
      case ItemSlot.Shoulder:
      case ItemSlot.Waist:
      case ItemSlot.Legs:
      case ItemSlot.Back:
      case ItemSlot.Feet:
      case ItemSlot.Chest:
      case ItemSlot.Wrist:
      case ItemSlot.Mainhand:
        return `uncommon`
      default:
        return `poor`
    }
  }

  get chanceOnHitList(): string[] {
    const arr: string[] = []

    return arr
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
