import Cast from '@/wow/class/Cast'
import ScoreInfo from '@/wow/interface/ScoreInfo'
import CastDmgObject from '@/wow/interface/CastDmgObject'
import CastPeriodicDmgObject from '@/wow/interface/CastPeriodicDmgObject'
import Item from '@/wow/class/Item'
import TrinketDisplayValues from '@/wow/interface/TrinketDisplayValues'
import { getTrinketEffectiveDuration, trinketDmgValues } from '@/wow/class/Trinkets'
import HasteDisplayValues from '@/wow/interface/HasteDisplayValues'
import QuicknessDisplayValues from '@/wow/interface/QuicknessDisplayValues'
import IgniteData from '@/wow/interface/IgniteData'
import CastDmgValues from '@/wow/interface/CastDmgValues'
import TrinketInfo from '@/wow/interface/TrinketInfo'
import {
  IGNITE_TICK_1_SCALING_FACTOR,
  IGNITE_TICK_2_SCALING_FACTOR,
  IGNITE_TICK_3_SCALING_FACTOR,
  IGNITE_TICK_4_SCALING_FACTOR,
  IGNITE_TICK_5_SCALING_FACTOR,
  INT_FOR_1_SPELL_CRIT_MAGE,
  SPELL_CRIT_CAP
} from '../module/Constants'

export default class DamageData {
  cast: Cast

  constructor(cast: Cast) {
    this.cast = cast
  }

  updateScoreInfo(scoreInfo: ScoreInfo) {
    /* XXX: Very hacky, but update the scoreInfo on the equipment to keep
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    a record we can reference elsewhere without needing to reprocess it */
    scoreInfo.targetType = this.cast.target.options.type
    scoreInfo.castDmgEV = this.expectedDmgValuePerCast()
    scoreInfo.castLag = this.cast.options.castLag
    scoreInfo.spellHitWeight = this.spellHitWeight
    scoreInfo.spellCritWeight = this.spellCritWeight
    scoreInfo.spellPenetrationWeight = this.spellPenetrationWeight
    scoreInfo.spellHaste = this.cast.character.getEffectiveSpellHaste()
    scoreInfo.spellBaseCastTime = this.cast.castTime
    scoreInfo.spellEffectiveCastTime = this.cast.effectiveCastTime()
    scoreInfo.spellCastTimeWithoutHaste = this.cast.effectiveCastTimeWithoutHaste
    scoreInfo.spellName = this.cast.spell.name
    scoreInfo.spellPower = this.cast.effectiveSpellPower
    scoreInfo.spellPowerToDmgRatio = this.spellPowerToDmgRatio
    scoreInfo.spellCoefficient = this.cast.spell.coefficient.direct
    scoreInfo.spellCrit = this.cast.character.getEffectiveSpellCrit(this.cast.magicSchoolForSpell, this.cast.spell.name)
    scoreInfo.effectiveDmgMultiplier = this.cast.effectiveDmgMultiplier
    scoreInfo.magicSchool = this.cast.magicSchoolForSpell

    scoreInfo.maxItemLevel = this.cast.options.maxItemlevel
    scoreInfo.itemType = this.cast.options.itemType
    scoreInfo.pvpItems = this.cast.options.pvpItems
    scoreInfo.encLen = this.cast.options.encLen
    scoreInfo.partialCasts = this.cast.options.partialCasts
    scoreInfo.targetType = this.cast.options.target.type
  }

  _dmgData = (dmgFn: Function, min: number, max: number, avg: number) => {
    const minDmg = dmgFn(min)
    const maxDmg = dmgFn(max)
    const avgDmg = dmgFn(avg)

    return {
      min: minDmg,
      max: maxDmg,
      avg: avgDmg,
      text: `${avgDmg.toFixed(0)} (${minDmg.toFixed(0)} - ${maxDmg.toFixed(0)})`
    }
  }

  normalDmg(bonusSP = 0): CastDmgObject {
    const dmgObj = {} as CastDmgObject

    const _baseDmg = (dmg: number) => {
      return dmg * this.cast.baseDmgMultiplier
    }

    const _actualDmg = (dmg: number) => {
      return dmg + this.cast.spell.coefficient.direct * (this.cast.effectiveSpellPower + bonusSP)
    }

    const _effectiveDmg = (dmg: number) => {
      return dmg * this.cast.effectiveDmgMultiplier
    }

    dmgObj.base = this._dmgData(_baseDmg, this.cast.spell.minDmg, this.cast.spell.maxDmg, this.cast.spell.avgDmg)
    dmgObj.actual = this._dmgData(_actualDmg, dmgObj.base.min, dmgObj.base.max, dmgObj.base.avg)
    dmgObj.effective = this._dmgData(_effectiveDmg, dmgObj.actual.min, dmgObj.actual.max, dmgObj.actual.avg)
    return dmgObj
  }

  critDmg(bonusSP = 0): CastDmgObject {
    const critDmgObj = {} as CastDmgObject
    const normalDmgObj = this.normalDmg(bonusSP)

    const _critDmg = (dmg: number) => {
      return dmg * this.cast.critMultiplier
    }

    critDmgObj.base = this._dmgData(_critDmg, normalDmgObj.base.min, normalDmgObj.base.max, normalDmgObj.base.avg)
    critDmgObj.actual = this._dmgData(
      _critDmg,
      normalDmgObj.actual.min,
      normalDmgObj.actual.max,
      normalDmgObj.actual.avg
    )
    critDmgObj.effective = this._dmgData(
      _critDmg,
      normalDmgObj.effective.min,
      normalDmgObj.effective.max,
      normalDmgObj.effective.avg
    )

    return critDmgObj
  }

  get periodicDmg(): CastPeriodicDmgObject {
    const periodicDmg = {} as CastPeriodicDmgObject

    const _periodicDmgData = (totalTickDmg: number) => {
      const tick = this.cast.spell.tickDmg > 0 ? totalTickDmg : 0
      const total = this.cast.spell.tickDmg > 0 ? tick * (this.cast.spell.duration / this.cast.spell.tickRate) : 0
      const initialLostUptime = (100 * (this.cast.spell.castTime + this.cast.spell.tickRate)) / this.cast.options.encLen
      const uptime = Math.round(10 * (this.cast.spell.uptime - initialLostUptime)) / 10
      const tickText = `${tick.toFixed(0)} every ${this.cast.spell.tickRate} sec`
      const totalText = `${total.toFixed(0)} over ${this.cast.spell.duration} sec`

      return {
        tick,
        total,
        uptime,
        tickText,
        totalText
      }
    }

    periodicDmg.base = _periodicDmgData(this.cast.spell.tickDmg * this.cast.baseDmgMultiplier)
    periodicDmg.actual = _periodicDmgData(
      periodicDmg.base.tick +
        (this.cast.spell.coefficient.periodic / this.cast.spell.ticks) * this.cast.effectiveSpellPower
    )
    periodicDmg.effective = _periodicDmgData(periodicDmg.actual.tick * this.cast.effectiveDmgMultiplier)

    return periodicDmg
  }

  get trinketInfo(): TrinketInfo {
    return {
      encLen: this.cast.options.encLen,
      magicSchool: this.cast.magicSchoolForSpell,
      targetType: this.cast.target.options.type,
      castDmgEV: this.expectedDmgValuePerCast(),
      castLag: this.cast.options.castLag,
      spellHaste: this.cast.character.getEffectiveSpellHaste(),
      spellBaseCastTime: this.cast.castTime,
      spellEffectiveCastTime: this.cast.effectiveCastTime(),
      spellPower: this.cast.effectiveSpellPower,
      spellPowerToDmgRatio: this.spellPowerToDmgRatio,
      spellCoefficient: this.cast.spell.coefficient.direct
    }
  }

  // any trinket that doesn't directly add spell power to each cast will be handled here
  get onUseTrinketEncounterDPS(): number {
    let dps = 0
    const trinket1 = this.cast.character.equipment.items.trinket
    if (trinket1) {
      const values = trinketDmgValues(trinket1.itemJSON, this.trinketInfo)
      if (values.onUse) {
        dps += values.onUse.encounterDPS
      }
    }
    const trinket2 = this.cast.character.equipment.items.trinket2
    if (trinket2) {
      const values = trinketDmgValues(trinket2.itemJSON, this.trinketInfo)
      if (values.onUse) {
        dps += values.onUse.encounterDPS
      }
    }

    return dps
  }

  _getTrinketDisplayValues(trinket: Item): TrinketDisplayValues[] {
    let displayValues = [] as TrinketDisplayValues[]

    const values = trinketDmgValues(trinket.itemJSON, this.trinketInfo)
    if (values.passive) {
      displayValues.push({
        name: values.name,
        type: 'passive',
        avgSPPerCast: values.passive.avgSPPerCast,
        encounterDPS: values.passive.encounterDPS,
        totalCasts: values.passive.totalCasts,
        totalDmg: values.passive.totalDmg,
        totalSP: values.passive.totalSP,
        uptime: values.passive.uptime
      })
    }
    if (values.onUse) {
      displayValues.push({
        name: values.name,
        type: 'on use',
        avgSPPerBuffedCast: values.onUse.avgSPPerBuffedCast,
        buffedCasts: values.onUse.buffedCasts,
        uptimeDPS: values.onUse.uptimeDPS,
        avgSPPerCast: values.onUse.avgSPPerCast,
        encounterDPS: values.onUse.encounterDPS,
        totalCasts: values.onUse.totalCasts,
        totalDmg: values.onUse.totalDmg,
        totalSP: values.onUse.totalSP,
        uptime: values.onUse.uptime
      })
    }
    return displayValues
  }

  get trinketDisplayValues(): TrinketDisplayValues[] {
    let displayValues = [] as TrinketDisplayValues[]

    const trinket1 = this.cast.character.equipment.items.trinket
    if (trinket1) {
      displayValues = displayValues.concat(this._getTrinketDisplayValues(trinket1))
    }

    const trinket2 = this.cast.character.equipment.items.trinket2
    if (trinket2) {
      displayValues = displayValues.concat(this._getTrinketDisplayValues(trinket2))
    }
    return displayValues
  }

  hasteDisplayValues(bonusHaste = 0): HasteDisplayValues {
    const hastePercent = this.cast.character.getEffectiveSpellHaste() + bonusHaste
    const totalCasts = this.numCasts(true)
    const baseCastTime = this.cast.castTime
    const castTimeReduction = (baseCastTime * hastePercent) / 100

    const castsWithoutHaste = this.numCastsWithoutHaste()
    const castsWithHaste = this.numCastsWithoutQuickness(false, bonusHaste)

    const igniteDmg = this.igniteData().totalExpectedDmg
    const igniteDmgWithHaste = this.igniteData(0, 0, hastePercent).totalExpectedDmg
    const addedIgniteDmg = igniteDmgWithHaste - igniteDmg

    // calculate additional casts
    const additionalCasts = Math.round(100 * (castsWithHaste - castsWithoutHaste)) / 100
    const addedSpellDmg = additionalCasts * this.expectedDmgValuePerCast(0, 0, 0, 0, false) // don't double count ignite

    const totalDmg = Math.round(addedSpellDmg + addedIgniteDmg)
    const totalSP = totalDmg / this.spellPowerToDmgRatio
    const avgSPPerCast = totalSP / totalCasts
    const spellDmgEncounterDPS = addedSpellDmg / this.cast.options.encLen
    const encounterDPS = totalDmg / this.cast.options.encLen

    return {
      hastePercent: hastePercent,
      castTimeReduction: castTimeReduction,
      castsWithoutHaste: castsWithoutHaste,
      castsWithHaste: castsWithHaste,
      additionalCasts: additionalCasts,
      addedIgniteDmg: addedIgniteDmg,
      addedSpellDmg: addedSpellDmg,
      totalDmg: totalDmg,
      avgSPPerCast: avgSPPerCast,
      spellDmgEncounterDPS: spellDmgEncounterDPS,
      encounterDPS: encounterDPS
    }
  }

  get quicknessDisplayValues(): QuicknessDisplayValues {
    const duration = 30
    const cooldown = 120

    const castTime = this.cast.effectiveCastTime()

    const castTimeWithoutLag = castTime - this.cast.options.castLag
    const castTimeWhileActive = castTimeWithoutLag / 1.06 + this.cast.options.castLag

    const numCastsStarted = Math.ceil(duration / castTimeWhileActive)
    const effectiveSingleDuration = numCastsStarted * castTimeWhileActive

    const uptime =
      Math.round(10 * getTrinketEffectiveDuration(effectiveSingleDuration, cooldown, this.cast.options.encLen)) / 10

    const effectiveHastePercent = Math.round(10 * 5 * (uptime / this.cast.options.encLen)) / 10

    let normalCasts = uptime / castTime
    let buffedCasts = uptime / castTimeWhileActive

    // calculate SP equivalents in terms of the original total amount of casts
    const increaseInCasts = Math.round(10 * (buffedCasts - normalCasts)) / 10

    const totalCastsWithoutQuickness = this.numCastsWithoutQuickness(true)
    const totalCastsWithQuickness = totalCastsWithoutQuickness + increaseInCasts

    // calculate damage added by the trinket
    const totalDmg = Math.round(increaseInCasts * this.expectedDmgValuePerCast())
    const dmgPerCast = totalDmg / this.numCastsWithoutQuickness(true)
    const avgSPPerCast = dmgPerCast / this.spellPowerToDmgRatio

    const encounterDPS = totalDmg / this.cast.options.encLen

    return {
      uptime: uptime,
      effectiveHastePercent: effectiveHastePercent,
      castTimeReduction: castTime - castTimeWhileActive,
      additionalCasts: increaseInCasts,
      partialCastsWithoutHaste: totalCastsWithoutQuickness,
      partialCastsWithHaste: totalCastsWithQuickness,
      totalDmg: totalDmg,
      avgSPPerCast: avgSPPerCast,
      encounterDPS: encounterDPS
    }
  }

  numCastsWithoutQuickness(includeTrinkets = false, bonusHaste = 0): number {
    const haste = this.cast.character.getEffectiveSpellHaste() + bonusHaste
    let numCasts = Math.round((100 * this.cast.options.encLen) / this.cast.effectiveCastTime(haste)) / 100
    // check for mpq

    if (includeTrinkets) {
      const trinketValues = this.trinketDisplayValues
      for (const value of trinketValues) {
        if (value.name === 'Mind Quickening Gem') {
          numCasts += value.buffedCasts ? value.buffedCasts : 0
        }
      }
    }

    return this.cast.options.partialCasts ? numCasts : Math.floor(numCasts)
  }

  numCastsWithoutHaste(): number {
    const numCasts = Math.round((100 * this.cast.options.encLen) / this.cast.effectiveCastTimeWithoutHaste) / 100
    return this.cast.options.partialCasts ? numCasts : Math.floor(numCasts)
  }

  numCasts(includeTrinkets = false, includeQuickness = false, bonusHaste = 0): number {
    let numCasts = this.numCastsWithoutQuickness(includeTrinkets, bonusHaste)

    if (this.cast.options.useQp && includeQuickness) {
      numCasts += this.quicknessDisplayValues.additionalCasts
    }

    return numCasts
  }

  get manaUsed(): number {
    return this.numCasts(true, true) * this.cast.manaCostPerCast
  }

  calculateExpectedTicks(igniteChance: number, biggerIgniteChances: number[]) {
    // chance of a new ignite tick is igniteChance - the cumulative chance of all bigger ignites
    const chanceOfFreshTick = igniteChance - biggerIgniteChances.reduce((a, b) => a + b, 0)
    // a fresh tick will cancel a previous 2nd tick, but need to consider 2nd tick if a fresh tick doesn't occur
    const chanceOfPrevious2ndTick = (1 - chanceOfFreshTick) * igniteChance
    return chanceOfFreshTick + chanceOfPrevious2ndTick
  }

  igniteData(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0): IgniteData {
    const uptimes = {
      ignite1: 0,
      ignite2: 0,
      ignite3: 0,
      ignite4: 0,
      ignite5: 0
    }

    const ticks = {
      ignite1: 0,
      ignite2: 0,
      ignite3: 0,
      ignite4: 0,
      ignite5: 0
    }

    const dmg = {
      ignite1: 0,
      ignite2: 0,
      ignite3: 0,
      ignite4: 0,
      ignite5: 0
    }

    const dps = {
      ignite1: 0,
      ignite2: 0,
      ignite3: 0,
      ignite4: 0,
      ignite5: 0
    }

    if (this.cast.spell.isFire) {
      const ignite1Chance = this.cast.percentChanceToCrit(bonusHit, bonusCrit)
      const ignite2Chance = ignite1Chance * ignite1Chance
      const ignite3Chance = ignite2Chance * ignite1Chance
      const ignite4Chance = ignite3Chance * ignite1Chance
      const ignite5Chance = ignite4Chance * ignite1Chance

      // precalculate expected tick uptime for each ignite stack
      // for each ignite stack calculate chance of a tick * 2 sec
      const ignite1ExpectedTickUptime = ignite1Chance * 2
      const ignite2ExpectedTickUptime = ignite2Chance * 2
      const ignite3ExpectedTickUptime = ignite3Chance * 2
      const ignite4ExpectedTickUptime = ignite4Chance * 2
      const ignite5ExpectedTickUptime = ignite5Chance * 2

      // precalculate tick chances for cast #6+
      const ignite1TickChanceCast6 =
        this.calculateExpectedTicks(ignite1Chance, [ignite2Chance, ignite3Chance, ignite4Chance, ignite5Chance]) *
        IGNITE_TICK_1_SCALING_FACTOR
      const ignite2TickChanceCast6 =
        this.calculateExpectedTicks(ignite2Chance, [ignite3Chance, ignite4Chance, ignite5Chance]) *
        IGNITE_TICK_2_SCALING_FACTOR
      const ignite3TickChanceCast6 =
        this.calculateExpectedTicks(ignite3Chance, [ignite4Chance, ignite5Chance]) * IGNITE_TICK_3_SCALING_FACTOR
      const ignite4TickChanceCast6 =
        this.calculateExpectedTicks(ignite4Chance, [ignite5Chance]) * IGNITE_TICK_4_SCALING_FACTOR
      const ignite5TickChanceCast6 = this.calculateExpectedTicks(ignite5Chance, []) * IGNITE_TICK_5_SCALING_FACTOR

      const critDmg = this.critDmg(bonusSP)
      const igniteTickDmg =
        critDmg.effective.avg * this.cast.character.igniteCritDmgPercentPerTick * this.cast.igniteDmgMultiplier

      const tickDmg = {
        ignite1: igniteTickDmg,
        ignite2: igniteTickDmg * 2,
        ignite3: igniteTickDmg * 3,
        ignite4: igniteTickDmg * 4,
        ignite5: igniteTickDmg * 5
      }

      // ignore last cast to simplify lost ticks at the end of the fight
      for (let i = 2; i < this.numCasts(false, false, bonusHaste) - 1; i++) {
        // loop through each cast and calculate uptime/tick expected values
        if (i === 2) {
          // cast 2, check for crit on cast 1
          uptimes.ignite1 += ignite1ExpectedTickUptime
          ticks.ignite1 += ignite1Chance
        } else if (i === 3) {
          // cast 3, check for crit on cast 1/2
          uptimes.ignite1 += ignite1ExpectedTickUptime
          uptimes.ignite2 += ignite2ExpectedTickUptime
          // chance of fresh tick 1 + chance of previous tick 2
          ticks.ignite1 += this.calculateExpectedTicks(ignite1Chance, [ignite2Chance]) * IGNITE_TICK_1_SCALING_FACTOR
          ticks.ignite2 += ignite2Chance
        } else if (i === 4) {
          // cast 4, check for crit on cast 1/2/3
          uptimes.ignite1 += ignite1ExpectedTickUptime
          uptimes.ignite2 += ignite2ExpectedTickUptime
          uptimes.ignite3 += ignite3ExpectedTickUptime
          ticks.ignite1 +=
            this.calculateExpectedTicks(ignite1Chance, [ignite2Chance, ignite3Chance]) * IGNITE_TICK_1_SCALING_FACTOR
          ticks.ignite2 += this.calculateExpectedTicks(ignite2Chance, [ignite3Chance]) * IGNITE_TICK_2_SCALING_FACTOR
          ticks.ignite3 += ignite3Chance
        } else if (i === 5) {
          // cast 5, check for crit on cast 1/2/3/4
          uptimes.ignite1 += ignite1ExpectedTickUptime
          uptimes.ignite2 += ignite2ExpectedTickUptime
          uptimes.ignite3 += ignite3ExpectedTickUptime
          uptimes.ignite4 += ignite4ExpectedTickUptime
          ticks.ignite1 +=
            this.calculateExpectedTicks(ignite1Chance, [ignite2Chance, ignite3Chance, ignite4Chance]) *
            IGNITE_TICK_1_SCALING_FACTOR
          ticks.ignite2 +=
            this.calculateExpectedTicks(ignite2Chance, [ignite3Chance, ignite4Chance]) * IGNITE_TICK_2_SCALING_FACTOR
          ticks.ignite3 += this.calculateExpectedTicks(ignite3Chance, [ignite4Chance]) * IGNITE_TICK_3_SCALING_FACTOR
          ticks.ignite4 += ignite4Chance
        } else {
          // cast 6+, calculate crits on previous 5 casts
          uptimes.ignite1 += ignite1ExpectedTickUptime
          uptimes.ignite2 += ignite2ExpectedTickUptime
          uptimes.ignite3 += ignite3ExpectedTickUptime
          uptimes.ignite4 += ignite4ExpectedTickUptime
          uptimes.ignite5 += ignite5ExpectedTickUptime
          ticks.ignite1 += ignite1TickChanceCast6
          ticks.ignite2 += ignite2TickChanceCast6
          ticks.ignite3 += ignite3TickChanceCast6
          ticks.ignite4 += ignite4TickChanceCast6
          ticks.ignite5 += ignite5TickChanceCast6
        }
      }
      // handle partial casts
      if (this.cast.options.partialCasts) {
        const partialCastAmount =
          this.numCasts(false, false, bonusHaste) - Math.floor(this.numCasts(false, false, bonusHaste))
        uptimes.ignite1 += ignite1ExpectedTickUptime * partialCastAmount
        uptimes.ignite2 += ignite2ExpectedTickUptime * partialCastAmount
        uptimes.ignite3 += ignite3ExpectedTickUptime * partialCastAmount
        uptimes.ignite4 += ignite4ExpectedTickUptime * partialCastAmount
        uptimes.ignite5 += ignite5ExpectedTickUptime * partialCastAmount
        ticks.ignite1 += ignite1TickChanceCast6 * partialCastAmount
        ticks.ignite2 += ignite2TickChanceCast6 * partialCastAmount
        ticks.ignite3 += ignite3TickChanceCast6 * partialCastAmount
        ticks.ignite4 += ignite4TickChanceCast6 * partialCastAmount
        ticks.ignite5 += ignite5TickChanceCast6 * partialCastAmount
      }

      // calculate dmg values for each stack
      dmg.ignite1 = ticks.ignite1 * tickDmg.ignite1
      dmg.ignite2 = ticks.ignite2 * tickDmg.ignite2
      dmg.ignite3 = ticks.ignite3 * tickDmg.ignite3
      dmg.ignite4 = ticks.ignite4 * tickDmg.ignite4
      dmg.ignite5 = ticks.ignite5 * tickDmg.ignite5

      // calculate dps values for each stack
      dps.ignite1 = dmg.ignite1 / this.cast.options.encLen
      dps.ignite2 = dmg.ignite2 / this.cast.options.encLen
      dps.ignite3 = dmg.ignite3 / this.cast.options.encLen
      dps.ignite4 = dmg.ignite4 / this.cast.options.encLen
      dps.ignite5 = dmg.ignite5 / this.cast.options.encLen

      const totalExpectedDmg =
        ticks.ignite1 * tickDmg.ignite1 +
        ticks.ignite2 * tickDmg.ignite2 +
        ticks.ignite3 * tickDmg.ignite3 +
        ticks.ignite4 * tickDmg.ignite4 +
        ticks.ignite5 * tickDmg.ignite5

      const totalExpectedTickDmg =
        totalExpectedDmg / (ticks.ignite1 + ticks.ignite2 + ticks.ignite3 + ticks.ignite4 + ticks.ignite5)

      return {
        uptimes: uptimes,
        ticks: ticks,
        tickDmg: tickDmg,
        dps: dps,
        dmg: dmg,
        totalExpectedTickDmg: totalExpectedTickDmg,
        totalExpectedDmg: totalExpectedDmg,
        totalDps: totalExpectedDmg / this.cast.options.encLen
      }
    } else {
      return {
        uptimes: uptimes,
        ticks: ticks,
        tickDmg: {
          ignite1: 0,
          ignite2: 0,
          ignite3: 0,
          ignite4: 0,
          ignite5: 0
        },
        dmg: dmg,
        dps: dps,
        totalExpectedTickDmg: 0,
        totalExpectedDmg: 0,
        totalDps: 0
      }
    }
  }

  dps(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0): CastDmgObject {
    const dmgObj = {} as CastDmgObject
    const normalDmgObj = this.normalDmg(bonusSP)
    const critDmgObj = this.critDmg(bonusSP)

    const percentChanceToNormal = this.cast.percentChanceToNormal(bonusHit, bonusCrit)
    const percentChanceToCrit = this.cast.percentChanceToCrit(bonusCrit)

    const _dps = (normalDmg: number, critDmg: number) => {
      return (
        (normalDmg * percentChanceToNormal + critDmg * percentChanceToCrit) / this.cast.effectiveCastTime(bonusHaste)
      )
    }

    const _dpsDmgData = (dpsFn: Function, normalDmgValues: CastDmgValues, critDmgValues: CastDmgValues) => {
      const minDps = dpsFn(normalDmgValues.min, critDmgValues.min)
      const maxDps = dpsFn(normalDmgValues.max, critDmgValues.max)
      const avgDps = dpsFn(normalDmgValues.avg, critDmgValues.avg)

      return {
        min: minDps,
        max: maxDps,
        avg: avgDps,
        text: `${avgDps.toFixed(0)} (${minDps.toFixed(0)} - ${maxDps.toFixed(0)})`,
        dps: avgDps
      }
    }

    dmgObj.base = _dpsDmgData(_dps, normalDmgObj.base, critDmgObj.base)
    dmgObj.actual = _dpsDmgData(_dps, normalDmgObj.actual, critDmgObj.actual)
    dmgObj.effective = _dpsDmgData(_dps, normalDmgObj.effective, critDmgObj.effective)

    return dmgObj
  }

  totalDPS(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0): number {
    let directDPS = this.dps(bonusHit, bonusCrit).effective.dps
    let periodicDPS = this.periodicDPS.effective.dps
    if (!directDPS) {
      directDPS = 0
      periodicDPS = 0
    } else if (!periodicDPS) {
      periodicDPS = 0
    }
    const igniteDPS = this.igniteData(bonusHit, bonusCrit, bonusHaste, bonusSP).totalDps
    const onUseTrinketDPS = this.onUseTrinketEncounterDPS
    // only use the spell dmg from haste as the added ignite dmg is already accounted for
    const hasteDPS = this.cast.hasHaste ? this.hasteDisplayValues(bonusHaste).spellDmgEncounterDPS : 0
    const quicknessDPS = this.cast.options.useQp ? this.quicknessDisplayValues.encounterDPS : 0

    return directDPS + periodicDPS + igniteDPS + onUseTrinketDPS + hasteDPS + quicknessDPS
  }

  totalDMG(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0): number {
    return this.totalDPS(bonusHit, bonusCrit, bonusHaste, bonusSP) * this.cast.options.encLen
  }

  get periodicDPS(): CastDmgObject {
    const dmgObj = {
      base: {} as CastDmgValues,
      actual: {} as CastDmgValues,
      effective: {} as CastDmgValues
    } as CastDmgObject

    const periodicDmgObj = this.periodicDmg

    dmgObj.base.dps = periodicDmgObj.base.total > 0 ? periodicDmgObj.base.total / this.cast.spell.duration : 0
    dmgObj.actual.dps = periodicDmgObj.actual.total > 0 ? periodicDmgObj.actual.total / this.cast.spell.duration : 0
    dmgObj.effective.dps =
      periodicDmgObj.effective.total > 0 ? periodicDmgObj.effective.total / this.cast.spell.duration : 0

    // incorporate uptime
    dmgObj.base.dps *= periodicDmgObj.base.uptime / 100
    dmgObj.actual.dps *= periodicDmgObj.actual.uptime / 100
    dmgObj.effective.dps *= periodicDmgObj.effective.uptime / 100

    dmgObj.base.text = `${dmgObj.base.dps.toFixed(0)}`
    dmgObj.actual.text = `${dmgObj.actual.dps.toFixed(0)}`
    dmgObj.effective.text = `${dmgObj.effective.dps.toFixed(0)}`

    return dmgObj
  }

  expectedDmgValuePerCast(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0, includeIgnite = true): number {
    const normalDmg = this.normalDmg(bonusSP).effective.avg
    let critDmg = this.critDmg(bonusSP).effective.avg
    let igniteEVPerCast = 0
    // if fire spell and using ignite, add ignite damage
    if (this.cast.spell.isFire && includeIgnite) {
      // get total ignite dmg
      igniteEVPerCast =
        this.igniteData(bonusHit, bonusCrit, bonusHaste, bonusSP).totalExpectedDmg /
        this.numCasts(false, false, bonusHaste)
    }

    const normalEVPerCast = normalDmg * this.cast.percentChanceToNormal(bonusHit, bonusCrit)
    const critEVPerCast = critDmg * this.cast.percentChanceToCrit(bonusHit, bonusCrit)

    return normalEVPerCast + critEVPerCast + igniteEVPerCast
  }

  totalExpectedDmg(bonusHit = 0, bonusCrit = 0, bonusHaste = 0, bonusSP = 0): number {
    const normalDmgPerCast = this.normalDmg(bonusSP).effective.avg
    let critDmgPerCast = this.critDmg(bonusSP).effective.avg

    let igniteDmg = 0
    // if fire spell and using ignite, add ignite damage
    if (this.cast.spell.isFire) {
      // get total ignite dmg
      igniteDmg = this.igniteData(bonusHit, bonusCrit, bonusHaste, bonusSP).totalExpectedDmg
    }

    const normalEVPerCast = normalDmgPerCast * this.cast.percentChanceToNormal(bonusHit, bonusCrit)
    const critEVPerCast = critDmgPerCast * this.cast.percentChanceToCrit(bonusHit, bonusCrit)

    const numCasts = this.numCasts(false, false, bonusHaste)
    const normalDmg = normalEVPerCast * numCasts
    const critDmg = critEVPerCast * numCasts
    return normalDmg + critDmg + igniteDmg
  }

  get spellPowerToDmgRatio() {
    const expectedDmg = this.expectedDmgValuePerCast()
    const expectedDmgPlus1SP = this.expectedDmgValuePerCast(0, 0, 0, 1)

    return expectedDmgPlus1SP - expectedDmg
  }

  /**7
   * spell crit weight i.e. the amount of spell power 1 point of crit is worth.
   */
  get spellCritWeight(): number {
    return this.cast.effectiveSpellCrit < SPELL_CRIT_CAP ? this.spellCritToSpellDamage() : 0
  }

  get spellHasteWeight(): number {
    return this.cast.castTime > 1.5 ? this.spellHasteToSpellDamage() : 0
  }

  /**
   * spell hit weight i.e. the amount of spell power 1 point of hit is worth.
   */
  get spellHitWeight(): number {
    return this.cast.chanceToMiss > 1 ? this.spellHitToSpellDamage() : 0
  }

  /**
   * int weight i.e. the amount of spell power 1 point of int is worth
   */
  get intWeight(): number {
    return this.spellCritWeight > 0 ? this.spellCritWeight / INT_FOR_1_SPELL_CRIT_MAGE : 0
  }

  /**
   * spell pen weight i.e the amount of spell power 1 point of spell penetration is worth
   */

  get spellPenetrationWeight(): number {
    // if the boss only has unmitigatable level based resistances left after spell pen, then it's value is 0
    if (this.cast.effectiveTargetResistance <= this.cast.targetResistanceFromLevel) {
      return 0
    }

    // B == Spell Base damage
    // m == base damage modifier (moonfury)
    // c == spell coefficient
    // P == Spellpower
    const m = 1
    const B = this.normalDmg().base.avg
    const c = this.cast.spell.coefficient.direct
    const P = this.cast.baseSpellPower
    const BossResist = this.cast.target.spellResistance
    const SpellPen = this.cast.spellPenetration
    const result = (m * B + c * P) / (c * (400 - (BossResist - SpellPen)))

    return result
  }

  spellCritToSpellDamage(): number {
    const numCasts = this.numCasts()
    let critBonusDamage = this.cast.critBonusMultiplier * this.normalDmg().effective.avg

    let critDmg = critBonusDamage * this.cast.percentChanceToCrit() * numCasts
    let critPlus1Dmg = critBonusDamage * this.cast.percentChanceToCrit(0, 1) * numCasts

    critDmg += this.igniteData().totalExpectedDmg
    critPlus1Dmg += this.igniteData(0, 1).totalExpectedDmg

    const dmgIncreasePerCast = (critPlus1Dmg - critDmg) / numCasts

    return dmgIncreasePerCast / this.spellPowerToDmgRatio
  }

  spellHitToSpellDamage(): number {
    const spellHitDmg = this.totalExpectedDmg()
    const spellHitPlus1Dmg = this.totalExpectedDmg(1)

    const dmgIncrease = (spellHitPlus1Dmg - spellHitDmg) / this.numCasts()

    return dmgIncrease / this.spellPowerToDmgRatio
  }

  spellHasteToSpellDamage(): number {
    const spellHasteDmg = this.totalExpectedDmg()
    const spellHastePlus1Dmg = this.totalExpectedDmg(0, 0, 1)

    const dmgIncrease = (spellHastePlus1Dmg - spellHasteDmg) / this.numCasts()

    return dmgIncrease / this.spellPowerToDmgRatio
  }
}
