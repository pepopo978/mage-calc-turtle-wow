import OptionsCharacter from './OptionsCharacter'
import OptionsTarget from './OptionsTarget'
import ItemSlot from '../enum/ItemSlot'
import { ObtainType } from './ItemJSON'

export default interface Options {
  pvpItems: boolean
  partialCasts: boolean
  encLen: number
  maxItemlevel: number | undefined
  itemType: ObtainType | undefined
  spellName: string
  itemSearchSlot?: ItemSlot
  enchantSearchSlot?: ItemSlot
  castLag: number
  character: OptionsCharacter
  target: OptionsTarget
  useMg: boolean
  useMp: boolean
  useQp: boolean
  useTea: boolean
  useEvo: boolean
  numVates: number
}
