import ItemQuality from '../enum/ItemQuality'
import ItemSlot from '../enum/ItemSlot'
import TargetType from '../enum/TargetType'
import Effects from './Effects'
import WeaponStats from './WeaponStats'

export enum ObtainType {
  Quest = 'Quest',
  Vendor = 'Vendor',
  Drop = 'Drop',
  WorldBoss = 'World Boss',
  Crafted = 'Crafted',
  ContainedIn = 'Contained In',
  ReagentFor = 'Reagent For',
  FishedIn = 'Fished In',
  ObjectiveOf = 'Objective Of',
  Other = 'Other'
}

// TODO use these once there is reliable location data on items
export enum RaidLocations {
  Naxxramas = 'Naxxramas',
  ZulGurub = "Zul'Gurub",
  BlackwingLair = 'Blackwing Lair',
  MoltenCore = 'Molten Core',
  OnyxiasLair = "Onyxia's Lair",
  RuinsOfAhnQiraj = "Ruins of Ahn'Qiraj",
  TempleOfAhnQiraj = "Temple of Ahn'Qiraj"
}

// TODO use these once there is reliable location data on items
export enum DungeonLocations {
  Kharazhan = 'Karazhan',
  StormwindVault = 'Stormwind Vault',
  BlackrockDepths = 'Blackrock Depths',
  BlackrockSpire = 'Blackrock Spire',
  HateforgeQuarry = 'Hateforge Quarry',
  Scholomance = 'Scholomance',
  Stratholme = 'Stratholme',
  DireMaul = 'Dire Maul',
  SunkenTemple = "Temple of Atal'Hakkar",
  Maraudon = 'Maraudon',
  Uldaman = 'Uldaman',
  ZulFarrak = "Zul'Farrak",
  RazorfenDowns = 'Razorfen Downs',
  RazorfenKraul = 'Razorfen Kraul',
  ScarletMonastery = 'Scarlet Monastery',
  ShadowfangKeep = 'Shadowfang Keep',
  TheDeadmines = 'The Deadmines',
  TheStockade = 'The Stockade',
  WailingCaverns = 'Wailing Caverns',
  BlackfathomDeeps = 'Blackfathom Deeps',
  Gnomeregan = 'Gnomeregan',
  RagefireChasm = 'Ragefire Chasm'
}

export enum PVPLocations {
  ArathiBasin = 'Arathi Basin',
  AlteracValley = 'Alterac Valley',
  WarsongGulch = 'Warsong Gulch'
}

export enum WorldBoss {
  Azuregos = 'Azuregos',
  Kazzaak = 'Kazzak',
  Emeriss = 'Emeriss',
  Lethon = 'Lethon',
  Taerar = 'Taerar',
  Ysondre = 'Ysondre',
  DarkReaver = 'Dark Reaver of Karazhan',
  NerubianOverseer = 'Nerubian Overseer',
  Ostarius = 'Ostarius'
}

export default interface ItemJSON {
  id: number
  name: string
  level: number
  parentUrl: string
  url: string
  icon: string
  quality: ItemQuality
  armorType: string
  slot: ItemSlot
  boe: boolean
  unique: boolean
  armor: number
  durability: number
  strength: number
  agility: number
  stamina: number
  intellect: number
  spirit: number
  fireRes: number
  natureRes: number
  frostRes: number
  shadowRes: number
  arcaneRes: number
  effects: Effects
  setName: string
  setBonuses: Record<number, Effects> // key = min items to get effects, value = effects
  setItems: Array<string>

  location: string
  obtainType: ObtainType
  obtainFrom: string
  seeAlsoNum: number
  pvp: boolean
  dropChance?: number

  allowableClasses: Array<string>

  weaponStats?: WeaponStats

  score?: number
  onUseScore?: number
  setBonusScore?: number
}

export function createDummyItemJSON(): ItemJSON {
  return {
    id: 0,
    parentUrl: '',
    url: '',
    name: '',
    level: 0,
    icon: '',
    quality: ItemQuality.Common,
    armorType: '',
    slot: ItemSlot.Head,
    boe: false,
    unique: false,
    armor: 0,
    durability: 0,
    strength: 0,
    agility: 0,
    stamina: 0,
    intellect: 0,
    spirit: 0,
    fireRes: 0,
    natureRes: 0,
    frostRes: 0,
    shadowRes: 0,
    arcaneRes: 0,
    effects: {
      mp5: 0,
      hp5: 0,
      healPower: 0,
      spellCrit: 0,
      spellHit: 0,
      spellPen: 0,
      spellVamp: 0,
      spellHaste: 0,
      spellPower: 0,
      frostSpellPower: 0,
      fireSpellPower: 0,
      arcaneSpellPower: 0,
      natureSpellPower: 0,
      shadowSpellPower: 0,
      targetTypes: TargetType.All,
      onUse: false,
      custom: []
    },
    setName: '',
    setBonuses: {},
    setItems: [],
    location: '',
    obtainType: ObtainType.Other,
    obtainFrom: '',
    seeAlsoNum: 0,
    pvp: false,
    allowableClasses: []
  }
}
