import TargetType from '../enum/TargetType'

export default interface Effects {
  mana?: number
  hp?: number
  intellect?: number
  stamina?: number
  spirit?: number
  agility?: number
  strength?: number
  armor?: number
  dodge?: number
  threatReductionPercent?: number
  movementSpeedPercent?: number
  manaRegenWhileCasting?: number

  mp5: number
  hp5: number
  healPower: number
  spellCrit: number
  spellHit: number
  spellPen: number
  spellHaste: number
  spellPower: number
  spellVamp: number
  frostSpellPower: number
  fireSpellPower: number
  arcaneSpellPower: number
  natureSpellPower: number
  shadowSpellPower: number
  targetTypes: TargetType
  onUse: boolean
  custom: Array<string>
}
