export default interface HasteInfo {
  encLen: number
  partialCasts: boolean
  castDmgEV: number
  spellPowerToDmgRatio: number
  spellHaste: number
  spellBaseCastTime: number
  spellEffectiveCastTime: number
  spellCastTimeWithoutHaste: number
  castLag: number
  spellName: string
}
