export default interface HasteDisplayValues {
  hastePercent: number
  castTimeReduction: number
  additionalCasts: number
  castsWithoutHaste: number
  castsWithHaste: number
  addedIgniteDmg: number
  addedSpellDmg: number
  totalDmg: number
  avgSPPerCast: number
  spellDmgEncounterDPS: number
  encounterDPS: number
}
