import ItemSlot from '../enum/ItemSlot'
import Effects from './Effects'

export default interface EnchantJSON {
  id: number
  effectId: number
  name: string
  slot: ItemSlot
  phase: number
  icon: string
  score: number
  text: string
  exploit?: boolean
  effects: Effects
}
