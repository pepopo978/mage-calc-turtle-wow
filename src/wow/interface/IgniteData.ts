export default interface IgniteData {
  uptimes: { ignite1: number; ignite2: number; ignite3: number; ignite4: number; ignite5: number }
  ticks: { ignite1: number; ignite2: number; ignite3: number; ignite4: number; ignite5: number }
  tickDmg: { ignite1: number; ignite2: number; ignite3: number; ignite4: number; ignite5: number }
  dmg: { ignite1: number; ignite2: number; ignite3: number; ignite4: number; ignite5: number }
  dps: { ignite1: number; ignite2: number; ignite3: number; ignite4: number; ignite5: number }
  totalExpectedTickDmg: number
  totalExpectedDmg: number
  totalDps: number
}
