export default interface ManaData {
  manaGemsUsed: number
  manaGemsRestored: number
  manaPotionsUsed: number
  manaPotionsRestored: number
  clearCastingManaSaved: number
  teasUsed: number
  teasRestored: number
  evocatesUsed: number
  evocatesRestored: number
  finalMana: number
  wentOOM: boolean
}
