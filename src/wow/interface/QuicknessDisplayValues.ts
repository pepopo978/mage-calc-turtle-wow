export default interface QuicknessDisplayValues {
  uptime: number
  effectiveHastePercent: number
  castTimeReduction: number
  additionalCasts: number
  partialCastsWithoutHaste: number
  partialCastsWithHaste: number
  totalDmg: number
  avgSPPerCast: number
  encounterDPS: number
}
