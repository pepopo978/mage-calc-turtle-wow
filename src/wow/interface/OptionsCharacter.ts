import Gender from '../enum/Gender'
import PlayableRace from '../enum/PlayableRace'
import PlayableClass from '../enum/PlayableClass'
import Buffs from '../enum/Buffs'

export default interface OptionsCharacter {
  level: number
  gender: Gender
  race: PlayableRace
  class: PlayableClass
  buffs: Buffs[]
  talents: {
    arcane: {
      arcaneSubtlety: number
      arcaneFocus: number
      improvedArcaneMissiles: number

      wandSpecialization: number
      magicAbsorption: number
      arcaneConcentration: number

      magicAttunement: number
      arcaneImpact: number
      arcaneResilience: number

      improvedManaShield: number
      improvedCounterspell: number
      arcaneMeditation: number

      presenceOfMind: number
      acceleratedArcana: number

      arcaneInstability: number
      arcanePotency: number

      arcanePower: number
      brillianceAura: number
    }
    fire: {
      improvedFireball: number
      impact: number

      ignite: number
      flameThrowing: number
      improvedFireBlast: number

      incinerate: number
      improvedFlamestrike: number
      pyroblast: number
      burningSoul: number

      improvedScorch: number
      improvedFireWard: number
      masterOfElements: number

      criticalMass: number
      blastWave: number

      firePower: number

      combustion: number
    }
    frost: {
      frostWarding: number
      improvedFrostbolt: number
      elementalPrecision: number

      iceShards: number
      frostbite: number
      improvedFrostNova: number
      permafrost: number

      piercingIce: number
      coldSnap: number
      improvedBlizzard: number

      arcticReach: number
      frostChanneling: number
      shatter: number

      iceBlock: number
      improvedConeOfCold: number

      wintersChill: number

      iceBarrier: number
    }
  }
}
