/* constants */
import TargetType from '../enum/TargetType'
import PlayableRace from '../enum/PlayableRace'
import PlayableClass from '../enum/PlayableClass'
import Gender from '../enum/Gender'
import Options from '../interface/Options'
import Buffs from '../enum/Buffs'
import { FrostDps } from './Specs'
import { ObtainType } from '../interface/ItemJSON'
import Debuff from '../enum/Debuffs'

export const Defaults: Options = {
  encLen: 120,
  maxItemlevel: undefined,
  itemType: ObtainType.Other,
  pvpItems: true,
  partialCasts: true,
  spellName: 'Frostbolt Rank 11',
  itemSearchSlot: undefined,
  enchantSearchSlot: undefined,
  castLag: 0.1, // Latency / server delay on each cast
  character: {
    level: 60,
    gender: Gender.Male,
    race: PlayableRace.Gnome,
    class: PlayableClass.Mage,
    talents: FrostDps,
    buffs: [Buffs.ArcaneBrilliance, Buffs.MageArmor]
  },
  target: {
    level: 63,
    type: TargetType.Undead,
    spellResistance: 0,
    shimmer: 0,
    debuffs: {}
  },
  useMg: true,
  useMp: true,
  useTea: true,
  useEvo: true,
  numVates: 0,
  useQp: false
}

Defaults.target.debuffs[Debuff.CurseOfElements] = 1
Defaults.target.debuffs[Debuff.WintersChill] = 1
Defaults.target.debuffs[Debuff.ScorchVulnerability] = 5
