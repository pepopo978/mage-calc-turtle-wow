export const FrostDps = {
  arcane: {
    arcaneSubtlety: 2,
    arcaneFocus: 3,
    improvedArcaneMissiles: 0,

    wandSpecialization: 0,
    magicAbsorption: 4,
    arcaneConcentration: 5,

    magicAttunement: 0,
    arcaneImpact: 3,
    arcaneResilience: 1,

    improvedManaShield: 0,
    improvedCounterspell: 0,
    arcaneMeditation: 3,

    presenceOfMind: 1,
    acceleratedArcana: 1,

    arcaneInstability: 3,
    arcanePotency: 0,

    arcanePower: 1,
    brillianceAura: 0
  },
  fire: {
    improvedFireball: 0,
    impact: 0,

    ignite: 0,
    flameThrowing: 0,
    improvedFireBlast: 0,

    incinerate: 0,
    improvedFlamestrike: 0,
    pyroblast: 0,
    burningSoul: 0,

    improvedScorch: 0,
    improvedFireWard: 0,
    masterOfElements: 0,

    criticalMass: 0,
    blastWave: 0,

    firePower: 0,

    combustion: 0
  },
  frost: {
    frostWarding: 0,
    improvedFrostbolt: 5,
    elementalPrecision: 3,

    iceShards: 5,
    frostbite: 0,
    improvedFrostNova: 0,
    permafrost: 0,

    piercingIce: 3,
    coldSnap: 0,
    improvedBlizzard: 0,

    arcticReach: 1,
    frostChanneling: 3,
    shatter: 0,

    iceBlock: 0,
    improvedConeOfCold: 0,

    wintersChill: 0,

    iceBarrier: 0
  }
}

export const FireDps = {
  arcane: {
    arcaneSubtlety: 2,
    arcaneFocus: 3,
    improvedArcaneMissiles: 0,

    wandSpecialization: 0,
    magicAbsorption: 0,
    arcaneConcentration: 5,

    magicAttunement: 0,
    arcaneImpact: 0,
    arcaneResilience: 0,

    improvedManaShield: 0,
    improvedCounterspell: 0,
    arcaneMeditation: 0,

    presenceOfMind: 0,
    acceleratedArcana: 0,

    arcaneInstability: 0,
    arcanePotency: 0,

    arcanePower: 0,
    brillianceAura: 0
  },
  fire: {
    improvedFireball: 5,
    impact: 0,

    ignite: 5,
    flameThrowing: 2,
    improvedFireBlast: 3,

    incinerate: 2,
    improvedFlamestrike: 2,
    pyroblast: 1,
    burningSoul: 2,

    improvedScorch: 3,
    improvedFireWard: 0,
    masterOfElements: 3,

    criticalMass: 3,
    blastWave: 1,

    firePower: 5,

    combustion: 1
  },
  frost: {
    frostWarding: 0,
    improvedFrostbolt: 0,
    elementalPrecision: 3,

    iceShards: 0,
    frostbite: 0,
    improvedFrostNova: 0,
    permafrost: 0,

    piercingIce: 0,
    coldSnap: 0,
    improvedBlizzard: 0,

    arcticReach: 0,
    frostChanneling: 0,
    shatter: 0,

    iceBlock: 0,
    improvedConeOfCold: 0,

    wintersChill: 0,

    iceBarrier: 0
  }
}

// Dunno what good spec for this is
export const ArcaneDps = {
  arcane: {
    arcaneSubtlety: 2,
    arcaneFocus: 5,
    improvedArcaneMissiles: 5,

    wandSpecialization: 0,
    magicAbsorption: 3,
    arcaneConcentration: 5,

    magicAttunement: 0,
    arcaneImpact: 3,
    arcaneResilience: 1,

    improvedManaShield: 0,
    improvedCounterspell: 2,
    arcaneMeditation: 3,

    presenceOfMind: 1,
    acceleratedArcana: 1,

    arcaneInstability: 3,
    arcanePotency: 3,

    arcanePower: 1,
    brillianceAura: 1
  },
  fire: {
    improvedFireball: 0,
    impact: 0,

    ignite: 0,
    flameThrowing: 0,
    improvedFireBlast: 0,

    incinerate: 0,
    improvedFlamestrike: 0,
    pyroblast: 0,
    burningSoul: 0,

    improvedScorch: 0,
    improvedFireWard: 0,
    masterOfElements: 0,

    criticalMass: 0,
    blastWave: 0,

    firePower: 0,

    combustion: 0
  },
  frost: {
    frostWarding: 0,
    improvedFrostbolt: 5,
    elementalPrecision: 3,

    iceShards: 0,
    frostbite: 0,
    improvedFrostNova: 0,
    permafrost: 0,

    piercingIce: 0,
    coldSnap: 0,
    improvedBlizzard: 0,

    arcticReach: 0,
    frostChanneling: 0,
    shatter: 0,

    iceBlock: 0,
    improvedConeOfCold: 0,

    wintersChill: 0,

    iceBarrier: 0
  }
}
