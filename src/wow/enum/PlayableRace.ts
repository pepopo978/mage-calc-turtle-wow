enum PlayableRace {
  Human = 1,
  Orc = 2,
  Dwarf = 3,
  NightElf = 4,
  Undead = 5,
  Tauren = 6,
  Gnome = 7,
  Troll = 8,
  Goblin = 9,
  HighElf = 10
}

export default PlayableRace
